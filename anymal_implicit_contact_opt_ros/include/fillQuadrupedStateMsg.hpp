#pragma once

#include <anymal_bear_ct_model/ANYmalWithContact.h>
#include <quadruped_msgs/QuadrupedState.h>

void fillQuadrupedStateMsg(quadruped_msgs::QuadrupedState& msg, const ct::models::ANYmal::ANYmalWithContact<double>::state_vector_t& state,
                           const ct::models::ANYmal::ANYmalWithContact<double>::control_vector_t& control) {
  using anymal_t = ct::models::ANYmal::ANYmalWithContact<double>;
  msg.state = quadruped_msgs::QuadrupedState::STATE_OK;

  // pose
  const auto r_IB = anymal_t::getPositionWorldToTrunkFromCtState(state);
  const auto q_IB = anymal_t::getQuaternionTrunkToWorldFromCtState(state);
  // msg.pose.header.stamp = rosTime;
  msg.pose.pose.position.x = r_IB(0);
  msg.pose.pose.position.y = r_IB(1);
  msg.pose.pose.position.z = r_IB(2);
  msg.pose.pose.orientation.w = q_IB.w();
  msg.pose.pose.orientation.x = q_IB.x();
  msg.pose.pose.orientation.y = q_IB.y();
  msg.pose.pose.orientation.z = q_IB.z();

  // twist
  const auto v_B = anymal_t::getLinearVelocityTrunkInWorldFromCtState(state);
  const auto omega_B = anymal_t::getAngularVelocityTrunkInTrunkFromCtState(state);
  // msg.twist.header.stamp = rosTime;
  msg.twist.twist.linear.x = v_B(0);
  msg.twist.twist.linear.y = v_B(1);
  msg.twist.twist.linear.z = v_B(2);
  msg.twist.twist.angular.x = omega_B(0);
  msg.twist.twist.angular.y = omega_B(1);
  msg.twist.twist.angular.z = omega_B(2);

  // joints
  const auto jointPositions = anymal_t::getJointPositionsFromCtState(state);
  const auto jointVelocities = anymal_t::getJointVelocitiesFromCtState(state);
  const auto jointNames = ct::models::ANYmal::urdfJointNames();

  msg.joints.header.stamp = msg.header.stamp;

  msg.joints.name.clear();
  msg.joints.position.clear();
  msg.joints.velocity.clear();
  msg.joints.effort.clear();

  msg.joints.name.reserve(jointNames.size());
  msg.joints.position.reserve(jointNames.size());
  msg.joints.velocity.reserve(jointNames.size());
  msg.joints.effort.reserve(jointNames.size());

  for (size_t j = 0; j < jointNames.size(); ++j) {
    msg.joints.name.push_back(jointNames[j]);
    msg.joints.position.push_back(jointPositions[j]);
    msg.joints.velocity.push_back(jointVelocities[j]);
    msg.joints.effort.push_back(40 * std::tanh(control[j] / 20.0));
  }
  msg.joints.acceleration.resize(jointNames.size());

  // contacts
  msg.contacts.resize(4);
}

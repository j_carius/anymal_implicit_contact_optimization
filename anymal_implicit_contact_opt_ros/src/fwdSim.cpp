#include "fillQuadrupedStateMsg.hpp"

#include <anymal_bear_ct_model/ANYmalWithContact.h>

#include <quadruped_msgs/QuadrupedState.h>
#include <rosbag/bag.h>

#include <boost/filesystem.hpp>

#include <signal.h>
// enable the following if you want to use the header directly (instead of compiled model)
#include <anymal_bear_ct_model/ANYmalWithContact-impl-dynamics.h>

using anymal_t = ct::models::ANYmal::ANYmalWithContact<double>;

volatile sig_atomic_t abort_flag = 0;
void catch_signal(int sig) {  // can be called asynchronously
  abort_flag = 1;             // set flag
}

int main(int argc, char* argv[]) {
  // std::cout << std::setprecision(10) << std::endl;
  boost::filesystem::path filePath(__FILE__);
  const std::string configPath = filePath.parent_path().parent_path().generic_string() + std::string{"/config/"};
  const std::string configFile = configPath + "fwdSim.info";

  using feedback_t = ct::core::FeedbackMatrix<anymal_t::STATE_DIM, anymal_t::CONTROL_DIM, double>;
  using controller_t = ct::core::StateFeedbackController<anymal_t::STATE_DIM, anymal_t::CONTROL_DIM, double>;
  constexpr size_t nSteps = 200;

  anymal_t anymal;

  feedback_t K;  // feedback matrix
  K.setZero();
  ct::core::loadMatrix(configFile, "K", K);

  rosbag::Bag bag;
  bag.open("anymal_traj.bag", rosbag::bagmode::Write);

  anymal_t::state_vector_t state, state_next;
  anymal_t::percussion_t percussions;

  ct::core::loadMatrix(configFile, "init_state", state);

  controller_t ctrl(controller_t::state_vector_array_t{nSteps, state},
                    controller_t::control_vector_array_t{nSteps, anymal_t::control_vector_t::Zero()},
                    controller_t::feedback_array_t{nSteps, K}, anymal.getTimeDiscretization());

  double time = 0.0;
  signal(SIGINT, catch_signal);
  for (size_t i = 0; i < nSteps; ++i) {
    anymal_t::control_vector_t control;
    ctrl.computeControl(state, 0.0, control);
    anymal.propagateControlledDynamicsExtended(state, 0, control, state_next, percussions);

    time += anymal.getTimeDiscretization();
    const auto rosTime = ros::Time(time);

    quadruped_msgs::QuadrupedState stateMsg;
    stateMsg.header.frame_id = "odom";
    stateMsg.header.stamp = rosTime;
    stateMsg.pose.header.frame_id = "odom";
    stateMsg.pose.header.stamp = rosTime;
    stateMsg.twist.header.frame_id = "odom";
    stateMsg.twist.header.stamp = rosTime;
    stateMsg.joints.header.stamp = rosTime;
    fillQuadrupedStateMsg(stateMsg, state, control);

    bag.write("/quadruped_state", rosTime, stateMsg);

    state = state_next;
    if (abort_flag) {
      std::cout << "WARNING: Stopped due to SIGINT signal received." << std::endl;
      break;
    }
  }

  const std::string filename = bag.getFileName();
  bag.close();
  std::cout << "Sucessfully written bag file " << filename << " .\n Done." << std::endl;

  return 0;
}

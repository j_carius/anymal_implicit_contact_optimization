
#include <anymal_bear_ct_model/ANYmalWithContact.h>
#include <anymal_implicit_contact_opt/anymal_implicit_contact_opt.hpp>
#include <anymal_implicit_contact_opt/augmented_random_search.hpp>
#include <anymal_implicit_contact_opt/serialized_state_feedback_controller.hpp>
#include "fillQuadrupedStateMsg.hpp"

#include <kindr_msgs/VectorAtPosition.h>
#include <quadruped_msgs/QuadrupedState.h>

#include <geometry_msgs/Vector3Stamped.h>
#include <rosbag/bag.h>
#include <std_msgs/Float64.h>

#include <signal.h>

#include <cereal/archives/binary.hpp>
#include <cereal/types/Eigen.hpp>
#include <cereal/types/vector.hpp>

volatile sig_atomic_t abort_flag = 0;
void catch_signal(int sig) {  // can be called asynchronously
  abort_flag = 1;             // set flag
}

using anymalOpt_t = anymal_implicit_contact_opt::ANYmalImplicitContactOpt;

void saveSolutionToBag(const anymalOpt_t& anymalOpt, rosbag::Bag& bag, double cost, std::string prefix = "");

void saveSolutionDMS(const ct::core::StateVectorArray<anymalOpt_t::STATE_DIM, double>& xArray,
                     const ct::core::ControlVectorArray<anymalOpt_t::CONTROL_DIM, double>& uArray, rosbag::Bag& bag);

int main(int argc, char* argv[]) {
  // save the file for later reference
  std::time_t time_now = std::time(nullptr);
  std::stringstream timeStream;
  timeStream << std::put_time(std::localtime(&time_now), "%y-%m-%d_%OH%OM%OS");
  std::string home = getenv("HOME");
  std::string file = __FILE__;
  std::experimental::filesystem::copy_file(file, home + "/.ros/" + timeStream.str() + "_bagWriter.cpp");

  const auto seed = time(nullptr);
  std::cout << "using seed " << seed << std::endl;
  srand(seed);

  ros::Time::init();
  rosbag::Bag bag;
  bag.open("anymal_traj.bag", rosbag::bagmode::Write);

  // --- augmented random search
  constexpr bool augmented_random_search = false;
  if (augmented_random_search) {
    Eigen::Matrix<double, 4 * 12, 1> params;
    params.setRandom();

    using SCALAR = double;

    anymal_implicit_contact_opt::termFH_p_t term_LF;  //(new anymal_implicit_contact_opt::termFH_t(scalingZ_LF, zRef));
    anymal_implicit_contact_opt::termFH_p_t term_RF;  //(new anymal_implicit_contact_opt::termFH_t(scalingZ_RF, zRef));
    anymal_implicit_contact_opt::termFH_p_t term_LH;  //(new anymal_implicit_contact_opt::termFH_t(scalingZ_LH, zRef));
    anymal_implicit_contact_opt::termFH_p_t term_RH;  //(new anymal_implicit_contact_opt::termFH_t(scalingZ_RH, zRef));

    std::shared_ptr<anymal_implicit_contact_opt::termFH_vec_t> termFH_vec(
        new anymal_implicit_contact_opt::termFH_vec_t({term_LF, term_RF, term_LH, term_RH}));
    std::shared_ptr<anymalOpt_t> anymalOpt(new anymalOpt_t());
    anymal_implicit_contact_opt::eval_params(params, termFH_vec, anymalOpt);
    saveSolutionToBag(*anymalOpt, bag, anymalOpt->getCost());
    bag.close();
    return 0;
  }
  // --- augmented random search end

  anymalOpt_t anymalOpt;

  anymalOpt.initialize();

  constexpr bool saveAllIterations = false;
  constexpr bool mpc = false;

  if (saveAllIterations) {
    signal(SIGINT, catch_signal);
    bool foundBetter = true;
    const int maxIterations = 300;
    int iter = 0;
    int foundNothingBetterCounter = 0;

    saveSolutionToBag(anymalOpt, bag, anymalOpt.getCost(), "/iteration/" + std::to_string(iter));

    while (foundBetter && ++iter < maxIterations) {
      foundBetter = anymalOpt.runIteration();
      saveSolutionToBag(anymalOpt, bag, anymalOpt.getCost(), "/iteration/" + std::to_string(iter));
      if (abort_flag) {
        std::cout << "WARNING: Stopped due to SIGINT signal received." << std::endl;
        break;
      }
    }

    if (iter == maxIterations) {
      std::cout << "WARNING: Reached maximum number of iterations";
    }
  } else if (mpc) {
    anymalOpt.runMPC(100);
  } else {
    anymalOpt.solve();
    // anymalOpt.solveDMS();
  }

  // always save final solution without prefix
  saveSolutionToBag(anymalOpt, bag, anymalOpt.getCost());
  // saveSolutionDMS(anymalOpt.dmsSolver_->getSolution().xSolution_, anymalOpt.dmsSolver_->getSolution().uSolution_, bag);

  anymalOpt_t::Solution_t solution;
  anymalOpt.getSolution(solution);
  anymal_implicit_contact_opt::SerializedStateFeedbackController serializedSolution(solution);

  std::ofstream os("out.cereal", std::ios::binary);
  cereal::BinaryOutputArchive archive(os);
  archive(serializedSolution);

  const std::string filename = bag.getFileName();
  bag.close();
  std::cout << "Bag file with anymal trajectory succssfully written to " << filename << " .\nDone." << std::endl;

  return 0;
}

void saveSolutionDMS(const ct::core::StateVectorArray<anymalOpt_t::STATE_DIM, double>& xArray,
                     const ct::core::ControlVectorArray<anymalOpt_t::CONTROL_DIM, double>& uArray, rosbag::Bag& bag) {
  anymalOpt_t::ANYmalWithContactD_t anymal;
  const auto dt = anymal.getTimeDiscretization();
  const auto jointNames = ct::models::ANYmal::urdfJointNames();

  quadruped_msgs::QuadrupedState state;
  state.state = quadruped_msgs::QuadrupedState::STATE_OK;

  static const ros::Time startTime = ros::Time::now();

  for (size_t i = 0; i < xArray.size(); i++) {
    const ros::Time rosTime = startTime + ros::Duration(i * dt);
    state.header.stamp = rosTime;
    fillQuadrupedStateMsg(state, xArray[i], uArray[i]);

    state.pose.header.stamp = rosTime;
    state.twist.header.stamp = rosTime;

    bag.write("/quadruped_state", rosTime, state);
  }
}

void saveSolutionToBag(const anymalOpt_t& anymalOpt, rosbag::Bag& bag, double cost, std::string prefix) {
  anymalOpt_t::Solution_t solution;
  anymalOpt.getSolution(solution);

  ct::core::DiscreteArray<anymalOpt_t::ANYmalWithContactD_t::percussion_t> percussionArray;
  anymalOpt.getPercussions(percussionArray);

  const auto time_trajectory = solution.time();
  const auto state_trajectory = solution.x_ref();
  const auto control_trajectory = solution.uff();

  static anymalOpt_t::ANYmalWithContactD_t anymal;
  const auto dt = anymal.getTimeDiscretization();
  const auto jointNames = ct::models::ANYmal::urdfJointNames();

  quadruped_msgs::QuadrupedState state;
  state.state = quadruped_msgs::QuadrupedState::STATE_OK;

  static const ros::Time startTime = ros::Time::now();

  for (size_t i = 0; i < time_trajectory.size() - 1; ++i) {
    // time
    const ros::Duration time_offset(time_trajectory[i]);
    const ros::Time rosTime = startTime + time_offset;
    state.header.stamp = rosTime;

    fillQuadrupedStateMsg(state, state_trajectory[i], control_trajectory[i]);

    state.pose.header.stamp = rosTime;
    state.twist.header.stamp = rosTime;

    // contacts
    const auto footHeights = anymal.footHeight(state_trajectory[i]);
    const auto footVelocities = anymal.linearFootVelocities(state_trajectory[i]);

    const std::vector<std::string> footNames{"LF_FOOT", "RF_FOOT", "LH_FOOT", "RH_FOOT"};
    const std::vector<std::string> footNamesShort{"lf", "rf", "lh", "rh"};

    quadruped_msgs::Contact contact;
    contact.header.stamp = rosTime;

    kindr_msgs::VectorAtPosition force_msg;
    force_msg.type = kindr_msgs::VectorAtPosition::TYPE_FORCE;
    force_msg.header.stamp = rosTime;
    force_msg.header.frame_id = "odom";

    // calculate foot positions
    Eigen::Matrix<double, 4, 4> T_world_trunk;
    T_world_trunk.setIdentity();
    T_world_trunk.template topLeftCorner<3, 3>() =
        anymalOpt_t::ANYmalWithContactD_t::getRotationMatrixTrunkToWorldFromCtState(state_trajectory[i]);
    T_world_trunk.template topRightCorner<3, 1>() =
        anymalOpt_t::ANYmalWithContactD_t::getPositionWorldToTrunkFromCtState(state_trajectory[i]);

    const auto joint_positions = anymalOpt_t::ANYmalWithContactD_t::getJointPositionsRobcogenFromCtState(state_trajectory[i]);

    iit::ANYmal::tpl::HomogeneousTransforms<typename iit::rbd::tpl::TraitSelector<double>::Trait> homTransforms;
    const Eigen::Matrix<double, 4, 4> T_trunk_LF_foot = homTransforms.fr_base_X_fr_LF_FOOT(joint_positions);
    const Eigen::Matrix<double, 4, 4> T_trunk_RF_foot = homTransforms.fr_base_X_fr_RF_FOOT(joint_positions);
    const Eigen::Matrix<double, 4, 4> T_trunk_LH_foot = homTransforms.fr_base_X_fr_LH_FOOT(joint_positions);
    const Eigen::Matrix<double, 4, 4> T_trunk_RH_foot = homTransforms.fr_base_X_fr_RH_FOOT(joint_positions);

    const Eigen::Matrix<double, 4, 4> T_world_LF_foot = T_world_trunk * T_trunk_LF_foot;
    const Eigen::Matrix<double, 4, 4> T_world_RF_foot = T_world_trunk * T_trunk_RF_foot;
    const Eigen::Matrix<double, 4, 4> T_world_LH_foot = T_world_trunk * T_trunk_LH_foot;
    const Eigen::Matrix<double, 4, 4> T_world_RH_foot = T_world_trunk * T_trunk_RH_foot;

    std::vector<Eigen::Matrix<double, 4, 4>> footPositions{T_world_LF_foot, T_world_RF_foot, T_world_LH_foot, T_world_RH_foot};

    state.contacts.clear();
    for (size_t footId = 0; footId < 4; ++footId) {
      contact.name = footNames[footId];
      contact.position.z = footHeights[footId];
      if (contact.position.z > 0.0) {
        contact.state = quadruped_msgs::Contact::STATE_OPEN;
      } else if (footVelocities.segment<3>(3 * footId).norm() < 1e-3) {
        contact.state = quadruped_msgs::Contact::STATE_CLOSED;
      } else {
        contact.state = quadruped_msgs::Contact::STATE_SLIPPING;
      }
      contact.wrench.force.x = percussionArray[i](3 * footId + 0) / dt;
      contact.wrench.force.y = percussionArray[i](3 * footId + 1) / dt;
      contact.wrench.force.z = percussionArray[i](3 * footId + 2) / dt;

      state.contacts.push_back(contact);

      force_msg.vector.x = contact.wrench.force.x;
      force_msg.vector.y = contact.wrench.force.y;
      force_msg.vector.z = contact.wrench.force.z;

      force_msg.position.x = footPositions[footId](0, 3);
      force_msg.position.y = footPositions[footId](1, 3);
      force_msg.position.z = contact.position.z;

      geometry_msgs::Vector3Stamped footVel;
      footVel.header.stamp = rosTime;
      footVel.vector.x = footVelocities(3 * footId + 0);
      footVel.vector.y = footVelocities(3 * footId + 1);
      footVel.vector.z = footVelocities(3 * footId + 2);

      bag.write(prefix + "/loco_ros/desired_contact_force_" + footNamesShort[footId], rosTime, force_msg);
      bag.write(prefix + "/foot_velocity_" + footNamesShort[footId], rosTime, footVel);
    }

    // calculate COP and COM position in world frame
    // loot at paper "Zmp support areas for multicontact mobility under frictional constraints"
    iit::ANYmal::dyn::InertiaProperties inertiaProps;
    Eigen::Vector4d base_to_com_in_base;
    base_to_com_in_base.head<3>() = iit::ANYmal::getWholeBodyCOM(inertiaProps, joint_positions, homTransforms);
    base_to_com_in_base(3) = 1.0;
    Eigen::Vector3d world_to_com_in_world = (T_world_trunk * base_to_com_in_base).head<3>();

    // effective pressure point of z contact forces
    const double totalZPercussion =
        percussionArray[i](3 * 0 + 2) + percussionArray[i](3 * 1 + 2) + percussionArray[i](3 * 2 + 2) + percussionArray[i](3 * 3 + 2);
    Eigen::Vector2d effectivePressurePoint_inWorld;
    effectivePressurePoint_inWorld.setZero();

    effectivePressurePoint_inWorld += footPositions[0].topRightCorner<2, 1>() * percussionArray[i](3 * 0 + 2);
    effectivePressurePoint_inWorld += footPositions[1].topRightCorner<2, 1>() * percussionArray[i](3 * 1 + 2);
    effectivePressurePoint_inWorld += footPositions[2].topRightCorner<2, 1>() * percussionArray[i](3 * 2 + 2);
    effectivePressurePoint_inWorld += footPositions[3].topRightCorner<2, 1>() * percussionArray[i](3 * 3 + 2);
    effectivePressurePoint_inWorld /= totalZPercussion;

    std::cout << "effectivePressurePoint_inWorld " << effectivePressurePoint_inWorld.transpose() << std::endl;
    std::cout << "world_to_com_in_world " << world_to_com_in_world.transpose() << std::endl;

    bag.write(prefix + "/quadruped_state", rosTime, state);
  }

  std_msgs::Float64 cost_msg;
  cost_msg.data = cost;
  bag.write(prefix + "/cost", startTime, cost_msg);
}

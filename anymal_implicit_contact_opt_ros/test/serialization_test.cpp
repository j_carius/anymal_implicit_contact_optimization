#define Debug
#ifdef NDEBUG
#undef NDEBUG
#endif NDEBUG

#include <anymal_implicit_contact_opt/anymal_implicit_contact_opt.hpp>
#include <anymal_implicit_contact_opt/serialized_state_feedback_controller.hpp>

#include <cereal/archives/binary.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/Eigen.hpp>

#include <gtest/gtest.h>

TEST(ANYmalImplicitContactOpt, SerializationTest) {

  using namespace anymal_implicit_contact_opt;

  constexpr int length = 177;

  ANYmalImplicitContactOpt::Solution_t::state_vector_array_t x_arr(length);
  ANYmalImplicitContactOpt::Solution_t::control_vector_array_t u_arr(length);
  ANYmalImplicitContactOpt::Solution_t::feedback_array_t K_arr(length);

  for(auto& x : x_arr){
    x.setRandom();
  }
  for(auto& u : u_arr){
    u.setRandom();
  }


  ANYmalImplicitContactOpt::Solution_t solOut(x_arr, u_arr, K_arr, 0.01);

  {
    // serialize the solution and write to disk
    SerializedStateFeedbackController serializedSolutionOut(solOut);
    std::ofstream os("serializationTest.cereal", std::ios::binary);
    cereal::BinaryOutputArchive archiveOut(os);
    archiveOut(serializedSolutionOut);
  }

  // read serialized solution again from disk
  SerializedStateFeedbackController serializedSolutionIn;
  std::ifstream is("serializationTest.cereal", std::ios::binary);
  cereal::BinaryInputArchive archiveIn(is);
  archiveIn(serializedSolutionIn);
  ANYmalImplicitContactOpt::Solution_t solIn = serializedSolutionIn;

  //check sizes
  ASSERT_TRUE(solOut.x_ref().size() == solIn.x_ref().size());
  ASSERT_TRUE(solOut.uff().size() == solIn.uff().size());
  ASSERT_TRUE(solOut.time().size() == solIn.time().size());

  // check values
  for (int i = 0; i < solOut.x_ref().size(); i++) {
    bool test = solOut.x_ref()[i].isApprox(solIn.x_ref()[i], 1e-10);
    EXPECT_TRUE(test);
  }
  for (int i = 0; i < solOut.uff().size(); i++) {
    bool test = solOut.uff()[i].isApprox(solIn.uff()[i], 1e-10);
    EXPECT_TRUE(test);
  }
  for (int i = 0; i < solOut.time().size(); i++) {
    EXPECT_DOUBLE_EQ(solOut.time()[i], solIn.time()[i]);
  }


}

int main(int argc, char **argv) {
  std::srand(std::time(nullptr));  // use current time as seed for random generator
  std::cout << std::setprecision(12);
  Eigen::setNbThreads(1);
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
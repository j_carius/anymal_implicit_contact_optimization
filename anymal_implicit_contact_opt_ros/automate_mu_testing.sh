#!/bin/bash

CATKIN_WS="/home/jcarius/anymal_ws"
PATCHFILE_PATH="/home/jcarius/git/anymal_bear_ct_model/anymal_bear_ct_model/"

cd $CATKIN_WS
echo "Working on" `pwd`

source devel/setup.bash

runID=0

for pFile in $PATCHFILE_PATH/patchfile_*.patch
do
  runID=$(($runID+1))

  echo "" 
  echo "runID" $runID
  echo "Working on patch" $pFile

  patch $CATKIN_WS/src/anymal_bear_ct_model/anymal_bear_ct_model/include/anymal_bear_ct_model/ANYmalWithContact-impl.h < $pFile

  echo "Building now..."

  catkin build anymal_bear_ct_model --no-deps --cmake-args -DGENERATE_CODE=ON -DUSE_CLANG=ON >> building_log_$runID.log
  rosrun anymal_bear_ct_model anymal_bear_ct_model_codegen >> building_log_$runID.log
  catkin build anymal_bear_ct_model --no-deps --cmake-args -DGENERATE_CODE=OFF -DUSE_CLANG=ON >> building_log_$runID.log
  catkin build anymal_implicit_contact_opt_ros anymal_implicit_contact_opt_ctrl >> building_log_$runID.log

  echo "Running the optimization..."
  rosrun anymal_implicit_contact_opt_ros bagWriter >> building_log_$runID.log

  mv anymal_traj.bag anymal_traj_$runID.bag
done

//
// Created by ruben on 13.08.18.
//

#pragma once

#include <loco_quadruped/common/LegsQuadruped.hpp>

#include <anymal_implicit_contact_opt/anymal_implicit_contact_opt.hpp>

namespace anymal_implicit_contact_opt_ctrl {

    struct Convert {
        using anymalOpt_t = anymal_implicit_contact_opt::ANYmalImplicitContactOpt;
        using anymal_t = ct::models::ANYmal::ANYmalWithContact<double>;
        using contact_flag_t = std::array<bool, 4>;


        static void setContactState(const contact_flag_t &stanceLegs,
                                    loco_quadruped::LegsQuadruped& legs)
        {
            using namespace loco;
            for (const auto& leg : legs) {
                auto id = leg->getId();

                if (stanceLegs.at(id)) {
                    leg->getContactSchedulePtr()->setShouldBeGrounded(true);
                    leg->getLimbStrategyPtr()->setLimbStrategyEnum(LimbStrategyEnum::Support);
                } else {
                    leg->getContactSchedulePtr()->setShouldBeGrounded(false);
                    leg->getLimbStrategyPtr()->setLimbStrategyEnum(LimbStrategyEnum::Motion);
                }
            }
        };



        static std::array<bool,4> getStanceLegs(const loco_quadruped::LegsQuadruped &legs){
            using namespace loco;
            std::array<bool,4> stanceLegs;
            for (auto leg : legs) {
                auto id = leg->getId();
                bool is_support           = leg->getLimbStrategy().getLimbStrategyEnum() == LimbStrategyEnum::Support;
                bool is_contact_invariant = leg->getLimbStrategy().getLimbStrategyEnum() == LimbStrategyEnum::ContactInvariant;
                stanceLegs.at(id) = is_support || is_contact_invariant;
            }
            return stanceLegs;
        };

        static void setTrajReferenceToLoco(double time, double groundHeight, anymal_t& anymal, const anymalOpt_t::Solution_t& sol,loco_quadruped::LegsQuadruped &legs,
                                           loco_quadruped::TorsoQuadruped &torso,
                                           loco::GaitPatternOcs2 &gaitPattern,
                                           const kindr::RotationQuaternionD& q_planToOdom,
                                           const kindr::Position3D& r_odom_plan_InOdom)
        {
            // NOTE: in this context world frame refers to odom
            auto x_ref_traj = sol.getReferenceStateTrajectory();
            auto u_ff_traj = sol.getFeedforwardTrajectory();



            constexpr double dt_numDiff = 0.005;

            auto x_ref = x_ref_traj.eval(time);
            auto u_ff = u_ff_traj.eval(time);


          using hom4x4Mat= Eigen::Matrix<double, 4, 4>;

          hom4x4Mat T_plan_trunk;
          T_plan_trunk.setIdentity();
          T_plan_trunk.template topLeftCorner<3, 3>() = anymalOpt_t::ANYmalWithContactD_t::getRotationMatrixTrunkToWorldFromCtState(x_ref);
          T_plan_trunk.template topRightCorner<3, 1>() = anymalOpt_t::ANYmalWithContactD_t::getPositionWorldToTrunkFromCtState(x_ref);

          const auto joint_positions = anymalOpt_t::ANYmalWithContactD_t::getJointPositionsRobcogenFromCtState(x_ref);
          iit::ANYmal::tpl::HomogeneousTransforms<typename iit::rbd::tpl::TraitSelector<double>::Trait> homTransforms;
          const hom4x4Mat T_trunk_LF_foot = homTransforms.fr_base_X_fr_LF_FOOT(joint_positions);
          const hom4x4Mat T_trunk_RF_foot = homTransforms.fr_base_X_fr_RF_FOOT(joint_positions);
          const hom4x4Mat T_trunk_LH_foot = homTransforms.fr_base_X_fr_LH_FOOT(joint_positions);
          const hom4x4Mat T_trunk_RH_foot = homTransforms.fr_base_X_fr_RH_FOOT(joint_positions);

          const Eigen::Vector3d r_plan_LF_foot_inPlan = (T_plan_trunk * T_trunk_LF_foot).topRightCorner<3,1>();
          const Eigen::Vector3d r_plan_RF_foot_inPlan = (T_plan_trunk * T_trunk_RF_foot).topRightCorner<3,1>();
          const Eigen::Vector3d r_plan_LH_foot_inPlan = (T_plan_trunk * T_trunk_LH_foot).topRightCorner<3,1>();
          const Eigen::Vector3d r_plan_RH_foot_inPlan = (T_plan_trunk * T_trunk_RH_foot).topRightCorner<3,1>();

          std::vector<Eigen::Vector3d> footPositions{r_plan_LF_foot_inPlan, r_plan_RF_foot_inPlan, r_plan_LH_foot_inPlan, r_plan_RH_foot_inPlan};

          double min_footheight = (*std::min_element(footPositions.begin(), footPositions.end(), [](const Eigen::Vector3d& a, const Eigen::Vector3d& b){return a(2) < b(2);}))(2);
          min_footheight = std::min(min_footheight, 0.0);

          groundHeight -= min_footheight;


          if(time > x_ref_traj.getTimeArray().back()){
            MELO_WARN_THROTTLE(1.0, "mpc time has reached end of trajectory.");
            std::array<bool, 4> stanceLegFlags{true, true, true, true};
            gaitPattern.setStanceLegs(stanceLegFlags);
            x_ref.tail<18>().setZero();
            u_ff.setZero();
          }

            const kindr::Position3D r_plan_trunk_inPlan(anymal_t::getPositionWorldToTrunkFromCtState(x_ref));
            const auto q_TrunkToPlan = anymal_t::getQuaternionTrunkToWorldFromCtState(x_ref);
            const auto v_trunk_in_plan = kindr::Velocity3D(anymal_t::getLinearVelocityTrunkInWorldFromCtState(x_ref));
            const auto omega_trunk_in_trunk = kindr::LocalAngularVelocityD(anymal_t::getAngularVelocityTrunkInTrunkFromCtState(x_ref));

            const auto a_trunk_in_plan = loco::LinearAcceleration(anymal_t::getLinearVelocityTrunkInWorldFromCtState(x_ref_traj.eval(time+dt_numDiff))
                - anymal_t::getLinearVelocityTrunkInWorldFromCtState(x_ref_traj.eval(time))) / dt_numDiff; // TODO use acceleration filter

//            std::cout << "ground height = " << groundHeight << std::endl;
//            std::cout << "a trunk " << a_trunk_in_plan.toImplementation().transpose() << std::endl;

            const auto& torsoMeas = torso.getMeasuredState();
            const auto& q_WorldToControl = torsoMeas.inControlFrame().getOrientationWorldToControl();
            const kindr::RotationQuaternionD q_planToControl = q_WorldToControl * q_planToOdom;

            // calculate setpoints in the correct frame
            kindr::RotationQuaternionD q_TrunkToControl = q_WorldToControl * q_planToOdom * q_TrunkToPlan;
            kindr::Position3D r_world_base_inWorld = r_odom_plan_InOdom + q_planToOdom.rotate(r_plan_trunk_inPlan);
            r_world_base_inWorld(2) += groundHeight;

//            std::cout << "r_odom_plan_InOdom: " << r_odom_plan_InOdom.toImplementation().transpose() << std::endl;
//            std::cout << "r_plan_trunk_inPlan: " << r_plan_trunk_inPlan.toImplementation().transpose() << std::endl;
//          std::cout << "desired torso pos in odom " << r_world_base_inWorld.toImplementation().transpose() << std::endl;

          // set base reference to torso
          auto torsoDes = torso.getDesiredStatePtr();
            torsoDes->setTargetPoint(loco::TorsoStateDesired::TargetPoint::BASE);

            torsoDes->setPositionWorldToBaseInWorldFrame(kindr::Position3D(r_world_base_inWorld));
            torsoDes->setLinearVelocityTargetInControlFrame(q_planToControl.rotate(v_trunk_in_plan));
            torsoDes->setLinearAccelerationTargetInControlFrame(q_planToControl.rotate(a_trunk_in_plan));

            torsoDes->setOrientationEulerAnglesZyxBaseToWorld(loco::EulerAnglesZyx(q_planToOdom * q_TrunkToPlan));
            torsoDes->setAngularVelocityBaseInControlFrame(omega_trunk_in_trunk);
            torsoDes->setAngularAccelerationTargetInControlFrame(loco::AngularAcceleration::Zero()); // TODO

            torsoDes->setOrientationControlToBase(q_TrunkToControl.inverted()); // this sets to goal orientation

            // set joint reference
            for (const auto& leg : legs) {
                auto id = leg->getId();
                loco::JointPositions q_des = x_ref.segment<3>(id*3 + anymal_t::gen_pos_joints_start_idx);
                loco::JointPositions dq_des = x_ref.segment<3>(id*3 + anymal_t::gen_pos_joints_start_idx + anymal_t::gen_vel_start_idx);
                loco::JointTorques tau = 40.0 * (u_ff.segment<3>(id*3)/20.0).array().tanh();
                leg->getLimbStateDesiredPtr()->setJointPositions(q_des);
                leg->getLimbStateDesiredPtr()->setJointVelocities(dq_des);
                leg->getLimbStateDesiredPtr()->setJointTorques(tau);
            }

            // set feet reference
            auto footVel_inPlan = anymal.linearFootVelocities(x_ref);


            for (const auto& leg : legs) {
                auto id = leg->getId();

                loco::Position r_plan_foot_inPlan(footPositions[id]);
                loco::Position r_odom_foot_inOdom = r_odom_plan_InOdom + q_planToOdom.rotate(r_plan_foot_inPlan);
                r_odom_foot_inOdom(2) += groundHeight;
                loco::LinearVelocity v_foot_inPlan(footVel_inPlan.segment<3>(3*id));
                loco::LinearVelocity v_foot_inWorld(q_planToOdom.rotate(v_foot_inPlan));
                loco::LinearAcceleration accWtoFDes_W(loco::LinearAcceleration::Zero()); // TODO
                leg->getFootPtr()->getStateDesiredPtr()->setPositionWorldToEndEffectorInWorldFrame(r_odom_foot_inOdom);
                leg->getFootPtr()->getStateDesiredPtr()->setLinearVelocityEndEffectorInWorldFrame(v_foot_inWorld);
                leg->getFootPtr()->getStateDesiredPtr()->setLinearAccelerationEndEffectorInWorldFrame(accWtoFDes_W);
            }

            std::array<bool, 4> stanceLegFlags{false, false, false, false};
            anymal_t::percussion_t P;
            anymal_t::state_vector_t state_next;
            anymal.propagateControlledDynamicsExtended(x_ref, 0, u_ff, state_next, P);
            for (const auto& leg : legs) {
                auto id = leg->getId();
                loco::Force contactForceDes_inPlan(P.segment<3>(3*id)/0.01);
                // Convention is: desired Forces onto the environment -> negative force
                leg->getFootPtr()->getStateDesiredPtr()->setForceAtEndEffectorInWorldFrame(-(q_planToOdom.rotate(contactForceDes_inPlan)));
//                std::cout << "contactForceDes_inPlan: " << contactForceDes_inPlan.toImplementation().transpose() << std::endl;
                if(contactForceDes_inPlan(2) > 10.0){
                  stanceLegFlags[id] = true;
                }
            }

//            std::cout << "stance leg flags: " << stanceLegFlags[0] << stanceLegFlags[1] << stanceLegFlags[2] << stanceLegFlags[3] << std::endl;


            gaitPattern.setStanceLegs(stanceLegFlags);
        }

    };

}

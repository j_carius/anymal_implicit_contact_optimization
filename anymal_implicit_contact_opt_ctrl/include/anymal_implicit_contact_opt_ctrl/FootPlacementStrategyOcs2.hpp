//
// Created by ruben on 16.08.18.
//

#pragma once

#include <loco/common/WholeBody.hpp>
#include <loco/common/TerrainModelBase.hpp>

namespace loco {

    class FootPlacementStrategyOcs2 {
    public:
        FootPlacementStrategyOcs2(WholeBody& wholeBody,
                                  TerrainModelBase& terrain);

        double regainVelocity_;

        bool initialize();

        bool advance(double dt);

    private:
        void regainContact(LegBase* leg, double dt);

        bool clipToConstraints( Position& positionWorldToDesiredFootInWorldFrame,
                                LinearVelocity& linearVelocityDesiredFootInWorldFrame,
                                LinearAcceleration& linearAccelerationDesiredFootInWorldFrame,
                                LegBase* leg);

        void setJointPositionAndVelocity(
                LegBase* leg,
                const Position& positionWorldToFootInWorldFrame,
                const LinearVelocity& linearVelocityDesFootInWorldFrame,
                const LinearAcceleration& linearAccelerationDesFootInWorldFrame);

        Position getPositionProjectedOnPlaneAlongSurfaceNormal(const Position& position);

        //! Reference to the torso.
        WholeBody& wholeBody_;

        //! Reference to the torso.
        TorsoBase& torso_;

        //! Reference to the legs.
        Legs& legs_;

        //! Reference to the terrain.
        TerrainModelBase& terrain_;
    };
}



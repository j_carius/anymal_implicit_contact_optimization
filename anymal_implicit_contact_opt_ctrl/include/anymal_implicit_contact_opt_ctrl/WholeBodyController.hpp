//
// Created by ruben on 26.08.18.
//

#pragma once

#include <loco_quadruped/motion_control/WholeBodyController.hpp>

namespace anymal_implicit_contact_opt_ctrl {
class WholeBodyControllerOcs2 : public loco_quadruped::WholeBodyController {
private:
    using Base = loco_quadruped::WholeBodyController;

public:
    using Base::RobotModel;
    using Base::RD;

    WholeBodyControllerOcs2(
        loco::WholeBody& wholeBody,
        const RobotModel& model,
        loco::TerrainModelBase& terrainModel);
    ~WholeBodyControllerOcs2() override = default;

protected:
    bool addOptimizationTasks() final;

private:
    bool addForceTrackingTask(int priority, double weight = 1.0);
};
} // namespace anymal_implicit_contact_opt_ctrl

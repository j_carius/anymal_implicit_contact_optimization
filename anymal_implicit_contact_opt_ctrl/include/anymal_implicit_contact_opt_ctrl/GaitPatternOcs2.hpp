//
// Created by ruben on 16.08.18.
//

#pragma once


#include <loco/gait_pattern/GaitPatternBase.hpp>
#include <loco/common/legs/Legs.hpp>

namespace loco {

class GaitPatternOcs2 : public GaitPatternBase {
public:
    GaitPatternOcs2(Legs& legs);
    virtual ~GaitPatternOcs2() = default;

    double getStrideDuration() const override;

    void setStrideDuration(double strideDuration) override;

    double getSwingPhaseForLeg(int iLeg) const override;

    double getSwingPhaseForLeg(int iLeg, double stridePhase) const override;

    double getStancePhaseForLeg(int iLeg) const override;

    double getStancePhaseForLeg(int iLeg, double stridePhase) const override;

    double getSwingDuration(int iLeg, double strideDuration) const override;

    double getStanceDuration(int iLeg, double strideDuration) const override;

    double getStanceDuration(int iLeg) const override;

    unsigned long getNGaitCycles() const override;

    int getNumberOfStanceLegs(double stridePhase) const override;

    double getTimeLeftInStance(int iLeg, double strideDuration, double stridePhase) const override;

    double getTimeLeftInSwing(int iLeg, double strideDuration, double stridePhase) const override;

    double getTimeSpentInStance(int iLeg, double strideDuration, double stridePhase) const override;

    double getTimeSpentInSwing(int iLeg, double strideDuration, double stridePhase) const override;

    double getTimeUntilNextStancePhase(int iLeg, double strideDuration, double stridePhase) const override;

    double getTimeUntilNextSwingPhase(int iLeg, double strideDuration, double stridePhase) const override;

    bool addVariablesToLog(bool update) override;

    bool shouldBeLegGrounded(int iLeg) const override;

    double getStridePhase() const override;

    void setStridePhase(double stridePhase) override;

    bool setToInterpolated(const GaitPatternBase &gaitPattern1, const GaitPatternBase &gaitPattern2, double t) override;

    void setSwingPhaseProgress(int iLeg, double stridePhase);

    void setStanceLegs(std::array<bool, 4>& contact_flags);

    void getStanceLegs(std::array<bool, 4>& contact_flags);

    bool advance(double dt) override;

    bool initialize(double dt) override;

private:
    //! Reference to the legs.
    Legs& legs_;

    std::array<double, 4> swingPhaseProgress_;
    std::array<bool, 4> contact_flags_;
};

}; // namespace loco

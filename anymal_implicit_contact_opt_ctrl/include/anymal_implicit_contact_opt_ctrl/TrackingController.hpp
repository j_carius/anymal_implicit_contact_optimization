#pragma once

#include <roco_ros/roco_ros.hpp>

#include <loco/event_detection/EventDetector.hpp>
#include <loco/limb_coordinator/LimbCoordinatorBase.hpp>
#include <robot_utils/filters/FirstOrderFilter.hpp>
#include <tinyxml_tools/tinyxml_tools.hpp>
#include "loco/common/TerrainModelBase.hpp"
#include "loco/common/TerrainModelFreePlane.hpp"
#include "loco/motion_control/MotionControllerBase.hpp"
#include "loco/terrain_perception/TerrainPerceptionBase.hpp"

#include <loco/heading_generation/HeadingGenerator.hpp>
#include "loco_quadruped/common/CommandTranslator.hpp"
#include "loco_quadruped/common/LegsQuadruped.hpp"
#include "loco_quadruped/common/WholeBodyQuadruped.hpp"

#include <anymal_roco/anymal_roco.hpp>
#include <roco/roco.hpp>

#include <memory>

#include <loco/mission_control/MissionControlSpeedFilter.hpp>

#include <std_srvs/SetBool.h>

// visualization
#include <loco_ros/visualization/ContactForces.hpp>
#include <loco_ros/visualization/Feet.hpp>
#include <loco_ros/visualization/Torso.hpp>
#include <loco_ros_quadruped/visualization/Ghost.hpp>

#include "GaitPatternOcs2.hpp"
#include "FootPlacementStrategyOcs2.hpp"
#include "ConversionsAnymalImplContactOpt.hpp"

#include <anymal_implicit_contact_opt/anymal_implicit_contact_opt.hpp>

namespace anymal_implicit_contact_opt_ctrl {

class TrackingCtrlImplicitContactOpt : virtual public roco_ros::ControllerRos<anymal_roco::RocoState, anymal_roco::RocoCommand> {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  using QD = quadruped_model::QuadrupedModel::QuadrupedDescription;
  using anymalOpt_t = anymal_implicit_contact_opt::ANYmalImplicitContactOpt;

  bool create(double dt) override final;
  bool initialize(double dt) override final;
  bool advance(double dt) override final;

  bool reset(double dt) override final { return initialize(dt); };
  bool cleanup() override final { return true; }
  bool stop() override final { return true; }
  bool preStop() override final;

 protected:
    TrackingCtrlImplicitContactOpt() = default;
  virtual ~TrackingCtrlImplicitContactOpt() override = default;

  double mpcTime_ = 0.0;
  double timeSinceLastMpcUpdate_ = 0.0;
  double minimumMpcRate_ = 0.150;
  double groundHeight_ = 0.0;
  double yawHistory_ = 0.0;

  std::unique_ptr<loco::GaitPatternOcs2> gaitPattern_;
  std::unique_ptr<loco::EventDetector> eventDetector_;
  std::unique_ptr<loco::LimbCoordinatorBase> limbCoordinator_;
  std::unique_ptr<loco::FootPlacementStrategyOcs2> footPlacementStrategy_;
  std::unique_ptr<loco_quadruped::WholeBodyQuadruped> wholeBody_;
  std::unique_ptr<loco_quadruped::LegsQuadruped> legs_;
  std::unique_ptr<loco_quadruped::TorsoQuadruped> torso_;
  std::unique_ptr<loco::TerrainModelFreePlane> terrainModel_;
  std::unique_ptr<loco::HeadingGenerator> headingGenerator_;
  std::unique_ptr<loco::MotionControllerBase> motionControl_;
  std::unique_ptr<loco::TerrainPerceptionBase> terrainPerception_;
  std::unique_ptr<loco_quadruped::CommandTranslator> commandTranslator_;
  std::unique_ptr<loco_quadruped::CommandTranslator> commandTranslatorSupportLeg_;
  std::unique_ptr<loco_quadruped::CommandTranslator> commandTranslatorSwingLeg_;

  std::array<Eigen::Vector3d, 4> desForceInWorld_;

 private:
  void initRos();
  bool initMotionReference();
  bool setDesiredLegAndTorsoState(double dt);
  bool advanceMeasurements(double dt);

  bool isRobotStateValid();

  bool workerCallback(const roco::WorkerEvent& event) const;
  void trackInitialState();
  void alignPlanWithStateEstimate();

  void initMRT();
  void shutdownMRT();
  bool updatePolicy();
  bool setMrtReference();
  void publishObservation();

  ros::NodeHandle nodeHandle_;
  roco::WorkerHandle publishWorkerHandle_;

  ct::models::ANYmal::ANYmalWithContact<double> anymal_;
  anymalOpt_t anymalOpt_;
  anymalOpt_t::Solution_t anymalOptSol_;

  ros::ServiceServer toggleJoystickServiceServer_;
  bool toggleJoystickServiceCallback(std_srvs::SetBool::Request& request, std_srvs::SetBool::Response& response);

  void filterAcceleration();
  std::unique_ptr<robot_utils::FirstOrderFilter<double>> terrainHeightFilter_;
  std::unique_ptr<robot_utils::FirstOrderFilter<loco::LinearAcceleration>> baseLinearFilter_;
  std::unique_ptr<robot_utils::FirstOrderFilter<loco::AngularAcceleration>> baseAngularFilter_;
  std::vector<std::unique_ptr<robot_utils::FirstOrderFilter<loco::LinearAcceleration>>> legFilters_;
  double footMaxAccelerationInG_;

  // target publishing
  bool useJoystick_ = true;
  std::unique_ptr<loco::MissionControlSpeedFilter> missionControl_;

  TiXmlHandle loadXMLConfig();
  TiXmlHandle loadXMLCommonConfig();
  double groundOffset_ = 0.0;
  bool useContactCorrection_ = true;
  double regainVelocity_ = -1.0;
  bool publishMeasuredContact_ = false;

  // visualize some components
  void visualizeStateInRviz() const;
  mutable loco_ros_quadruped::Ghost desiredStateVisualizer_;
  mutable loco_ros::Torso torsoVisualizer_;
  mutable loco_ros::Feet feetVisualizer_;
  mutable loco_ros::ContactForces contactForceVisualizer_;

  kindr::RotationQuaternionD q_planToOdom_;
  kindr::Position3D r_odom_plan_InOdom_;
};

}  // namespace anymal_implicit_contact_opt_ctrl

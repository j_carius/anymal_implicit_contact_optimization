//
// Created by ruben on 17.08.18.
//

#include "anymal_implicit_contact_opt_ctrl/FootPlacementStrategyOcs2.hpp"

namespace loco {

    FootPlacementStrategyOcs2::FootPlacementStrategyOcs2(WholeBody& wholeBody,
                                                         TerrainModelBase& terrain) : wholeBody_(wholeBody),
                                                                                      torso_(*wholeBody.getTorsoPtr()),
                                                                                      legs_(*wholeBody.getLegsPtr()),
                                                                                      terrain_(terrain) {};

    bool FootPlacementStrategyOcs2::initialize() {
        for (auto leg : legs_) {
            const Position& positionWorldToHipAtLiftOffInWorldFrame = leg->getStateLiftOff()->getPositionWorldToHipInWorldFrame();
            const Position& positionWorldToHipOnTerrainAlongNormalAtLiftOffInWorldFrame = getPositionProjectedOnPlaneAlongSurfaceNormal(positionWorldToHipAtLiftOffInWorldFrame);
            leg->getStateLiftOff()->setPositionWorldToHipOnTerrainAlongWorldZInWorldFrame(positionWorldToHipOnTerrainAlongNormalAtLiftOffInWorldFrame);
        }

        regainVelocity_ = 1.0;
    }

    bool FootPlacementStrategyOcs2::advance(double dt) {
        for (auto leg : legs_) {
            // Decide what to do based on the current state.
            if ((leg->getLimbStrategy().getLimbStrategyEnum() == LimbStrategyEnum::Motion) ||
                (leg->getLimbStrategy().getLimbStrategyEnum() == LimbStrategyEnum::ContactRecovery)) {
                switch(leg->getStateSwitcher().getState()) {
                    case(StateSwitcher::States::StanceSlipping): {
                        MELO_INFO_THROTTLE_STREAM(0.5, "Regain contact (slipping) for " << leg->getName());
                        regainContact(leg, dt);
                    } break;
                    case(StateSwitcher::States::StanceLostContact): {
                        MELO_INFO_THROTTLE_STREAM(0.5, "Regain contact (lost contact) for " << leg->getName());
                        regainContact(leg, dt);
                    } break;
                    case(StateSwitcher::States::SwingExpectingContact): {
                        MELO_INFO_THROTTLE_STREAM(0.5, "Regain contact (expecting) for " << leg->getName());
                        regainContact(leg, dt);
                    } break;

                    case(StateSwitcher::States::SwingNormal):
                    case(StateSwitcher::States::SwingLateLiftOff):
                    case(StateSwitcher::States::SwingBumpedIntoObstacle):
                        continue; break;

                    default: break;
                }
            }
        }
    }

    void FootPlacementStrategyOcs2::regainContact(LegBase* leg, double dt){
        // Compute regain trajectory.
        Position positionWorldToFootInWorldFrame =
                leg->getFoot().getStateMeasured().getPositionWorldToEndEffectorInWorldFrame() -
                regainVelocity_*dt*(loco::Position)Vector::UnitZ();
        LinearVelocity desiredLinearVelocityInWorldFrame(-regainVelocity_*Vector::UnitZ());
        LinearAcceleration linearAccelerationDesiredFootInWorldFrame = LinearAcceleration::Zero();

        // Clip to constraint.
        clipToConstraints(
                positionWorldToFootInWorldFrame,
                desiredLinearVelocityInWorldFrame,
                linearAccelerationDesiredFootInWorldFrame,
                leg);

        // Set joint state.
        setJointPositionAndVelocity(leg,
                                    positionWorldToFootInWorldFrame,
                                    desiredLinearVelocityInWorldFrame,
                                    linearAccelerationDesiredFootInWorldFrame);


        // Set torques (to zero for safety, are overwritten by WBC).
        auto torques = leg->getLimbStateDesiredPtr()->getJointTorques();
        torques.setZero();
        leg->getLimbStateDesiredPtr()->setJointTorques(torques);
    }

    bool FootPlacementStrategyOcs2::clipToConstraints(
            Position& positionWorldToDesiredFootInWorldFrame,
            LinearVelocity& linearVelocityDesiredFootInWorldFrame,
            LinearAcceleration& linearAccelerationDesiredFootInWorldFrame,
            LegBase* leg) {

        const auto positionBaseToLinkBaseInWorldFrame =
                wholeBody_.getTorso().getMeasuredState().getOrientationWorldToBase().inverseRotate(
                        leg->getLinks().get(1).getBaseToLinkPositionInBaseFrame());

        const auto positionWorldToThighInWorldFrame =
                wholeBody_.getTorso().getMeasuredState().getPositionWorldToBaseInWorldFrame()
                + positionBaseToLinkBaseInWorldFrame;

        const auto positionThighToDesiredFootholdInWorldFrame = positionWorldToDesiredFootInWorldFrame - positionWorldToThighInWorldFrame;

        // Avoid leg over extension.
        double maxLegExtension = leg->getLegProperties().getMaximumLimbExtension()*0.90;
        if (positionThighToDesiredFootholdInWorldFrame.norm() > maxLegExtension) {
            positionWorldToDesiredFootInWorldFrame = positionWorldToThighInWorldFrame + positionThighToDesiredFootholdInWorldFrame.normalized()*maxLegExtension;
            linearVelocityDesiredFootInWorldFrame.setZero();
            linearAccelerationDesiredFootInWorldFrame.setZero();
            MELO_WARN_THROTTLE_STREAM(0.1, "[FootPlacementStrategyOptimized::clipToConstraints] Avoid leg over-extension.\n");
        }

        return true;
    }

    void FootPlacementStrategyOcs2::setJointPositionAndVelocity(
            LegBase* leg,
            const Position& positionWorldToFootInWorldFrame,
            const LinearVelocity& linearVelocityDesFootInWorldFrame,
            const LinearAcceleration& linearAccelerationDesFootInWorldFrame) {

        auto footDesiredState = leg->getFootPtr()->getStateDesiredPtr();
        auto limbDesiredState = leg->getLimbStateDesiredPtr();
        const RotationQuaternion& orientationWorldToBase = torso_.getMeasuredState().getOrientationWorldToBase();

        // Set desired state.
        footDesiredState->setPositionWorldToEndEffectorInWorldFrame(positionWorldToFootInWorldFrame);
        footDesiredState->setLinearVelocityEndEffectorInWorldFrame(linearVelocityDesFootInWorldFrame);
        footDesiredState->setLinearAccelerationEndEffectorInWorldFrame(linearAccelerationDesFootInWorldFrame);

        // Set joint position.
        limbDesiredState->setJointPositions(
                leg->getEndEffectorPtr()->getJointPositionsFromPositionBaseToEndEffectorInBaseFrame(
                        orientationWorldToBase.rotate(positionWorldToFootInWorldFrame - torso_.getMeasuredState().getPositionWorldToBaseInWorldFrame())
                )
        );

        // Set joint velocity.
        limbDesiredState->setJointVelocities(
                leg->getEndEffectorPtr()->getJointVelocitiesFromLinearVelocityBaseToEndEffectorInBaseFrame(
                        orientationWorldToBase.rotate(linearVelocityDesFootInWorldFrame)
                        - torso_.getMeasuredState().getLinearVelocityBaseInBaseFrame()
                        - LinearVelocity(torso_.getMeasuredState().getAngularVelocityBaseInBaseFrame().toImplementation().cross(
                                leg->getFoot().getStateMeasured().getPositionBaseToEndEffectorInBaseFrame().toImplementation()))
                )
        );
    }

    Position FootPlacementStrategyOcs2::getPositionProjectedOnPlaneAlongSurfaceNormal(const Position& position) {
        return terrain_.getPositionProjectedOnPlaneAlongSurfaceNormalInWorldFrame(position);
    }
} // namespace loco
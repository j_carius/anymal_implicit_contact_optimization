
#include "anymal_implicit_contact_opt_ctrl/TrackingController.hpp"
#include <anymal_implicit_contact_opt_ctrl/ConversionsAnymalImplContactOpt.hpp>
#include <anymal_implicit_contact_opt_ctrl/WholeBodyController.hpp>
#include <loco/common/ParameterSet.hpp>
#include <loco/limb_coordinator/LimbCoordinatorDynamicGait.hpp>
#include <loco/terrain_perception/TerrainPerceptionFreePlane.hpp>
#include <message_logger/message_logger.hpp>

#include "loco/common/ParameterSet.hpp"
#include "loco/common/WholeBodyProperties.hpp"

#include "loco_quadruped/loco_quadruped.hpp"
#include "loco_quadruped/motion_control/WholeBodyController.hpp"

#include <loco_quadruped/heading_generation/HeadingGeneratorQuadruped.hpp>

#include "rocoma_plugin/rocoma_plugin.hpp"

#include <anymal_implicit_contact_opt/serialized_state_feedback_controller.hpp>

#include <cereal/archives/binary.hpp>
#include <cereal/types/Eigen.hpp>
#include <cereal/types/vector.hpp>

ROCOMA_EXPORT_CONTROLLER_ROS(TrackingCtrlImplicitContactOpt, anymal_roco::RocoState, anymal_roco::RocoCommand,
                             anymal_implicit_contact_opt_ctrl::TrackingCtrlImplicitContactOpt)

namespace anymal_implicit_contact_opt_ctrl {

bool TrackingCtrlImplicitContactOpt::create(double dt) {
  boost::shared_lock<boost::shared_mutex> lock(this->getStateMutex());

  auto& quadrupedModel = *getState().getQuadrupedModelPtr();
  auto& quadrupedModelDesired = *getState().getDesiredQuadrupedModelPtr();

  legs_ = loco_quadruped::make_unique_leg_group(quadrupedModel, quadrupedModelDesired);
  torso_.reset(new loco_quadruped::TorsoQuadruped("torso", quadrupedModel));
  wholeBody_.reset(new loco_quadruped::WholeBodyQuadruped(quadrupedModel, *torso_, *legs_, true));

  terrainModel_.reset(new loco::TerrainModelFreePlane());

  headingGenerator_.reset(new loco_quadruped::HeadingGeneratorQuadruped(*wholeBody_));

  terrainPerception_.reset(new loco::TerrainPerceptionFreePlane(*terrainModel_, *wholeBody_, *headingGenerator_));

  commandTranslator_.reset(new loco_quadruped::CommandTranslator());
  commandTranslatorSupportLeg_.reset(new loco_quadruped::CommandTranslator());
  commandTranslatorSwingLeg_.reset(new loco_quadruped::CommandTranslator());

  motionControl_.reset(new WholeBodyControllerOcs2(*wholeBody_, quadrupedModel, *terrainModel_));

  gaitPattern_.reset(new loco::GaitPatternOcs2(*wholeBody_->getLegsPtr()));
  eventDetector_.reset(new loco::EventDetector(*wholeBody_->getLegsPtr()));
  limbCoordinator_.reset(new loco::LimbCoordinatorDynamicGait(*wholeBody_, *gaitPattern_, false));
  footPlacementStrategy_.reset(new loco::FootPlacementStrategyOcs2(*wholeBody_, *terrainModel_));

  return true;
}

bool TrackingCtrlImplicitContactOpt::initialize(double dt) {
  if (!isRobotStateValid()) {
    MELO_FATAL("The robot state is not valid!");
    return false;
  }

  for (auto leg : *legs_.get()) {
    leg->initialize(dt);
    leg->getContactSchedulePtr()->setShouldBeGrounded(true);
    leg->getLimbStrategyPtr()->setLimbStrategyEnum(loco::LimbStrategyEnum::Support);
  }

  torso_->initialize(dt);
  // should actually track the CoM for dynamic consistency, but base for kinematic consistency
  // WBCOM is what the TOWR is planning for using Centroidal Dynamics
  torso_->getDesiredStatePtr()->setTargetPoint(loco::TorsoStateDesired::TargetPoint::BASE);

  const TiXmlHandle config_handle = loadXMLConfig();

  wholeBody_->loadParameters(config_handle);
  wholeBody_->initialize(dt);

  terrainModel_->loadParameters(config_handle);
  terrainModel_->initialize(dt);
  terrainPerception_->initialize(dt);

  motionControl_->loadParameters(config_handle);
  motionControl_->initialize(dt);

  anymal_implicit_contact_opt::SerializedStateFeedbackController serializedSol;
  std::ifstream is("/home/jcarius/catkin_ws/out.cereal", std::ios::binary);
  cereal::BinaryInputArchive archive(is);
  archive(serializedSol);

  anymalOptSol_ = serializedSol;

  //      anymalOpt_.initialize();
  //      anymalOpt_.solve();
  //      anymalOpt_.getSolution(anymalOptSol_);

  alignPlanWithStateEstimate();

  std::cout << "[Tracking Controller] Initialized r_odom_plan_inOdom to " << r_odom_plan_InOdom_.toImplementation().transpose()
            << std::endl;

  // Filter joint torques
  //      const int joint_torque_filt_window = 5;
  auto& u_traj = anymalOptSol_.getFeedforwardTrajectory();
  //      const auto u_traj_old = anymalOptSol_.getFeedforwardTrajectory(); // make copy (easy hack instead of recursion)
  constexpr double alpha = 0.2;
  auto u_filt = u_traj[0];
  for (int i = 0; i < u_traj.size(); i++) {
    u_traj[i] = alpha * u_traj[i] + (1.0 - alpha) * u_filt;
    u_filt = u_traj[i];
  }

  // Set joint gains
  TiXmlHandle pidGainsHandle = config_handle;
  if (!tinyxml_tools::getChildHandle(pidGainsHandle, config_handle, "PIDGains")) {
    return false;
  }
  // Load command parameter (control gains).
  if (!commandTranslatorSupportLeg_->loadParameters(pidGainsHandle, "SupportLegCommands")) {
    MELO_FATAL("[Crawling::initialize] Could not load command parameters for support leg!");
    return false;
  }
  if (!commandTranslatorSwingLeg_->loadParameters(pidGainsHandle, "SwingLegCommands")) {
    MELO_FATAL("[Crawling::initialize] Could not load command parameters for swing leg!");
    return false;
  }

  bool success = initMotionReference();
  initRos();

  // Add variables to log
  motionControl_->addVariablesToLog();
  try {
    dynamic_cast<loco::TerrainModelFreePlane*>(terrainModel_.get())->addVariablesToLog(true);
  } catch (...) {
  }
  for (auto leg : *legs_) {
    leg->addVariablesToLog(leg->getName());
  }
  torso_->addVariablesToLog();

  signal_logger::add(desForceInWorld_[0], "desForceInWorld_LF_foot");
  signal_logger::add(desForceInWorld_[1], "desForceInWorld_RF_foot");
  signal_logger::add(desForceInWorld_[2], "desForceInWorld_LH_foot");
  signal_logger::add(desForceInWorld_[3], "desForceInWorld_RH_foot");


  return success;
}

void TrackingCtrlImplicitContactOpt::alignPlanWithStateEstimate() {
  // get the transform from optimization world frame to odom
  const auto& r_odom_base_inOdom = torso_->getMeasuredState().getPositionWorldToBaseInWorldFrame();
  const auto& q_odomToBase = torso_->getMeasuredState().getOrientationWorldToBase();
  kindr::Position3D r_plan_base_inPlan(anymalOptSol_.getReferenceStateTrajectory().front().head<3>());
  auto q_baseToPlan = Convert::anymal_t::getQuaternionTrunkToWorldFromCtState(anymalOptSol_.getReferenceStateTrajectory().front());

  q_planToOdom_ = (q_baseToPlan * q_odomToBase).inverted();
  r_odom_plan_InOdom_ = r_odom_base_inOdom - q_planToOdom_.rotate(r_plan_base_inPlan);
  r_odom_plan_InOdom_(2) = 0.0;  // we use the ground height estimator later

  //      // adjust just that the ground height is correct (assumes LF Foot is on the ground)
  //      using hom4x4Mat= Eigen::Matrix<double, 4, 4>;
  //
  //      hom4x4Mat T_plan_trunk;
  //      T_plan_trunk.setIdentity();
  //      T_plan_trunk.template topLeftCorner<3, 3>() =
  //      anymalOpt_t::ANYmalWithContactD_t::getRotationMatrixTrunkToWorldFromCtState(anymalOptSol_.getReferenceStateTrajectory().front());
  //      T_plan_trunk.template topRightCorner<3, 1>() =
  //      anymalOpt_t::ANYmalWithContactD_t::getPositionWorldToTrunkFromCtState(anymalOptSol_.getReferenceStateTrajectory().front());
  //
  //      const auto joint_positions =
  //      anymalOpt_t::ANYmalWithContactD_t::getJointPositionsRobcogenFromCtState(anymalOptSol_.getReferenceStateTrajectory().front());
  //      iit::ANYmal::tpl::HomogeneousTransforms<typename iit::rbd::tpl::TraitSelector<double>::Trait> homTransforms;
  //      const hom4x4Mat T_trunk_LF_foot = homTransforms.fr_base_X_fr_LF_FOOT(joint_positions);
  //      const Eigen::Vector3d r_plan_LF_foot_inPlan = (T_plan_trunk * T_trunk_LF_foot).topRightCorner<3,1>();
  //
  //      Eigen::Vector3d r_odom_lfFoot_inOdom =
  //      getState().getQuadrupedModel().getPositionWorldToBody(quadruped_model::RD::BodyEnum::LF_FOOT,
  //      quadruped_model::RD::CoordinateFrameEnum::WORLD); r_odom_plan_InOdom_(2) = r_odom_lfFoot_inOdom(2) -
  //      q_planToOdom_.rotate(r_plan_LF_foot_inPlan)(2);
}

bool TrackingCtrlImplicitContactOpt::advance(double dt) {
  bool success = true;

  success = advanceMeasurements(dt) && success;
  // split the following method in gait pattern setting and leg/cmo setting.
  success = setDesiredLegAndTorsoState(dt) && success;

  success = motionControl_->advance(dt) && success;

  // Send commands
  {
    boost::unique_lock<boost::shared_mutex> lock(getCommandMutex());
    for (auto leg : *legs_) {
      const auto limbEnum = quadruped_model::RD::mapEnums<quadruped_model::RD::LimbEnum>(leg->getBranch());
      if ((leg->getLimbStrategy().getLimbStrategyEnum() == loco::LimbStrategyEnum::Support) ||
          (leg->getLimbStrategy().getLimbStrategyEnum() == loco::LimbStrategyEnum::ContactInvariant)) {
        commandTranslatorSupportLeg_->setPidGains(limbEnum, getCommand());
      } else {
        commandTranslatorSwingLeg_->setPidGains(limbEnum, getCommand());
      }
    }
    // Sets joint pos, vel and torque, doesn't matter which mode the leg is in
    commandTranslatorSupportLeg_->setCommands(getCommand(), wholeBody_->getLegs());
  }

  if (mpcTime_ > anymalOptSol_.getReferenceStateTrajectory().getTimeArray().back()) {
    MELO_WARN("resetting mpc time.");
    return false;
    mpcTime_ = anymalOptSol_.getReferenceStateTrajectory().getTimeArray().front();
    alignPlanWithStateEstimate();
  }


  return success;
}

bool TrackingCtrlImplicitContactOpt::advanceMeasurements(double dt) {
  if (!isRobotStateValid()) {
    MELO_FATAL("The robot state is not valid!");
    return false;
  }

  for (auto leg : *legs_) {
    leg->advance(dt);
  }

  torso_->advance(dt);
  wholeBody_->advance(dt);
  terrainPerception_->advance(dt);  // updates control frame

  return true;
}

TiXmlHandle TrackingCtrlImplicitContactOpt::loadXMLConfig() {
  // static so returned xml handle can still be used after function returns.
  static loco::ParameterSet parameterSet;
  std::string parameterFile = getParameterPath() + "/ControlParameters.xml";

  if (!parameterSet.loadXmlDocument(parameterFile)) MELO_ERROR_STREAM("Could not load parameter file: " << parameterFile);

  return parameterSet.getHandle().FirstChild("LocomotionController");
}

bool TrackingCtrlImplicitContactOpt::isRobotStateValid() {
  boost::shared_lock<boost::shared_mutex> lock(getStateMutex());

  return getState().getStatus() == anymal_description::StateStatus::STATUS_OK;
}

void TrackingCtrlImplicitContactOpt::initRos() {
  nodeHandle_ = getNodeHandle();
  desiredStateVisualizer_.initialize(nodeHandle_, "/ghost_desired/quadruped_state");
  feetVisualizer_.initialize(nodeHandle_, false);
  torsoVisualizer_.initialize(nodeHandle_, "/loco_ros/torso");
  contactForceVisualizer_.initialize(nodeHandle_, {"lf", "rf", "lh", "rh"});

  roco::WorkerOptions publishWorkerOptions;
  publishWorkerOptions.autostart_ = false;
  publishWorkerOptions.frequency_ = 20.0;
  publishWorkerOptions.name_ = "opt_ros_publisher";
  publishWorkerOptions.priority_ = 0;
  publishWorkerOptions.callback_ = boost::bind(&TrackingCtrlImplicitContactOpt::workerCallback, this, _1);
  publishWorkerHandle_ = addWorker(publishWorkerOptions);
  startWorker(publishWorkerHandle_);

  toggleJoystickServiceServer_ =
      nodeHandle_.advertiseService("toggle_joystick", &TrackingCtrlImplicitContactOpt::toggleJoystickServiceCallback, this);
}

bool TrackingCtrlImplicitContactOpt::initMotionReference() {
  mpcTime_ = 0.0;
  timeSinceLastMpcUpdate_ = 0.0;

  auto xmlCommonConfig = loadXMLCommonConfig();

  // groundOffset
  TiXmlHandle groundOffsetHandle = xmlCommonConfig;
  if (!tinyxml_tools::getChildHandle(groundOffsetHandle, xmlCommonConfig, "groundOffset")) {
    return false;
  }
  if (!tinyxml_tools::loadParameter(groundOffset_, groundOffsetHandle, "z")) {
    return false;
  }
  std::cout << "ground offset: " << groundOffset_ << std::endl;

  // Contact Handeling
  TiXmlHandle contactHandlingHandle = xmlCommonConfig;
  if (!tinyxml_tools::getChildHandle(contactHandlingHandle, xmlCommonConfig, "contactHandling")) {
    return false;
  }
  if (!tinyxml_tools::loadParameter(useContactCorrection_, contactHandlingHandle, "useContactCorrection")) {
    return false;
  }
  if (!tinyxml_tools::loadParameter(regainVelocity_, contactHandlingHandle, "regainVelocity")) {
    return false;
  }
  if (!tinyxml_tools::loadParameter(publishMeasuredContact_, contactHandlingHandle, "publishMeasuredContact")) {
    return false;
  }

  publishMeasuredContact_ &= useContactCorrection_;  // Can only publish filtered contacts if loco logic is active
  std::cout << "useContactCorrection: " << useContactCorrection_ << std::endl;
  std::cout << "regainVelocity: " << regainVelocity_ << std::endl;
  std::cout << "publishMeasuredContact: " << publishMeasuredContact_ << std::endl;

  // Terrain
  terrainModel_->getHeight(torso_->getMeasuredState().getPositionWorldToBaseInWorldFrame(), groundHeight_);
  groundHeight_ += groundOffset_;

  yawHistory_ = 0.0;

  const double dt = 1.0 / 400.0;
  gaitPattern_->initialize(dt);
  footPlacementStrategy_->initialize();
  footPlacementStrategy_->regainVelocity_ = regainVelocity_;

  // Filters
  double tau_terrain;
  double tau_base;
  double tau_feet;
  TiXmlHandle filtersHandle = xmlCommonConfig;
  if (!tinyxml_tools::getChildHandle(filtersHandle, xmlCommonConfig, "filters")) {
    return false;
  }
  if (!tinyxml_tools::loadParameter(tau_terrain, filtersHandle, "terrrainHeightTimeConstant")) {
    return false;
  }
  if (!tinyxml_tools::loadParameter(tau_base, filtersHandle, "feetAccelerationTimeConstant")) {
    return false;
  }
  if (!tinyxml_tools::loadParameter(tau_feet, filtersHandle, "baseAccelerationTimeConstant")) {
    return false;
  }
  if (!tinyxml_tools::loadParameter(footMaxAccelerationInG_, filtersHandle, "footMaxAccelerationInG")) {
    return false;
  }

  terrainHeightFilter_.reset(new robot_utils::FirstOrderFilter<double>(dt, tau_terrain, 1.0, groundHeight_));
  baseLinearFilter_.reset(new robot_utils::FirstOrderFilter<loco::LinearAcceleration>(dt, tau_base, 1.0, loco::LinearAcceleration::Zero()));
  baseAngularFilter_.reset(
      new robot_utils::FirstOrderFilter<loco::AngularAcceleration>(dt, tau_base, 1.0, loco::AngularAcceleration::Zero()));

  using LinearAccelerationFilter = robot_utils::FirstOrderFilter<loco::LinearAcceleration>;
  legFilters_.clear();
  legFilters_.reserve(legs_->size());
  for (auto k = 0; k < legs_->size(); ++k) {
    legFilters_.emplace_back(
        std::unique_ptr<LinearAccelerationFilter>(new LinearAccelerationFilter(dt, tau_feet, 1.0, loco::LinearAcceleration::Zero())));
  }

  // mission control
  TiXmlHandle commandsHandle = xmlCommonConfig;
  if (!tinyxml_tools::getChildHandle(commandsHandle, xmlCommonConfig, "commands")) {
    return false;
  }
  if (!tinyxml_tools::loadParameter(useJoystick_, commandsHandle, "useJoystick")) {
    return false;
  }

  missionControl_.reset(new loco::MissionControlSpeedFilter());
  static loco::ParameterSet parameterSet;
  std::string parameterFile = getParameterPath() + "/Mission.xml";
  if (!parameterSet.loadXmlDocument(parameterFile)) MELO_ERROR_STREAM("Could not load parameter file: " << parameterFile);
  missionControl_->loadParameters(parameterSet.getHandle());

  // More logging
  signal_logger::add(groundHeight_, "/groundHeight", "/ocs2_common", "m");
  signal_logger::add(timeSinceLastMpcUpdate_, "/timeSinceLastMpcUpdate", "/ocs2_common", "s");
  return true;
}

bool TrackingCtrlImplicitContactOpt::setDesiredLegAndTorsoState(double dt) {
  // Update terrain height measurement
  double rawTerrainHeight;
  terrainModel_->getHeight(torso_->getMeasuredState().getPositionWorldToBaseInWorldFrame(), rawTerrainHeight);
  rawTerrainHeight += groundOffset_;

  // Filter terrain
  terrainHeightFilter_->advance(rawTerrainHeight);
  //  groundHeight_ = terrainHeightFilter_->getFilteredValue();

  Convert::setTrajReferenceToLoco(mpcTime_, groundHeight_, anymal_, anymalOptSol_, *legs_, *torso_, *gaitPattern_, q_planToOdom_,
                                  r_odom_plan_InOdom_);

  int legNumber = 0;
  for(const auto leg : *legs_){
      desForceInWorld_[legNumber++] = leg->getFootPtr()->getStateDesiredPtr()->getForceAtEndEffectorInWorldFrame().toImplementation();
    }

  // set joint Positions to computed IK between meas hip -> des foot
  // otherwise joint PIDs dominate the foot tracking

  //  auto& quadrupedModel = *getState().getQuadrupedModelPtr();
  //  const auto& pos_world_base_inWorld = torso_->getMeasuredState().getPositionWorldToBaseInWorldFrame();
  //  const auto& q_worldToBase = torso_->getMeasuredState().getOrientationWorldToBase();
  //
  //  {
  //    loco::JointPositions jointPos(3);
  //    std::array<bool, 4> stanceLegDesired;
  //    gaitPattern_->getStanceLegs(stanceLegDesired);
  //
  //    for (const auto contactKey : quadruped_model::RD::getContactKeys()) {
  //      const auto contactEnum = contactKey.getEnum();
  //      const auto limbEnum = quadruped_model::RD::mapEnums<quadruped_model::RD::LimbEnum, quadruped_model::RD::ContactEnum>(
  //          contactEnum);
  //      const auto limbId = quadruped_model::RD::mapKeyEnumToKeyId(limbEnum);
  //      const auto leg = legs_->getPtr(limbId);
  //
  //      if(stanceLegDesired[limbId]){
  //        continue;
  //      }
  //
  //      const auto &pos_world_foot_inWorld = leg->getFoot().getStateDesired().getPositionWorldToEndEffectorInWorldFrame();
  //      const loco::Position pos_base_foot_inBase = q_worldToBase.rotate(
  //          -pos_world_base_inWorld + pos_world_foot_inWorld);
  //      if (!quadrupedModel.getLimbJointPositionsFromContactEnumIteratively(
  //          jointPos,
  //          pos_base_foot_inBase.toImplementation(),
  //          contactEnum)) {
  //        std::cout << "IK not converged for pos "
  //                  << pos_base_foot_inBase.toImplementation().transpose()
  //                  << std::endl;
  //        continue;
  //      }
  //      leg->getLimbStateDesiredPtr()->setJointPositions(jointPos);
  //    }
  //  }

  //

  //    for (int ileg = 0; ileg < 4; ileg++) {
  //      gaitPattern_->setSwingPhaseProgress(ileg, mrtPtr_->getsSwingPhaseProgress(ileg));
  //    }

  filterAcceleration();

  // Set gait pattern in the legs from internal state in gaitPattern_
  gaitPattern_->advance(dt);

  if (useContactCorrection_) {
    eventDetector_->advance(dt);  // checks for early contact/liftoff, slippage

    limbCoordinator_->advance(dt);  // sets limb strategy (support, contact recovery, motion, ...) based on eventDetector

    footPlacementStrategy_->advance(dt);  // reads limb strategy and potentially modifies setpoints/motion plan to regain contact
  } else {
    std::array<bool, 4> planned_contact;
    gaitPattern_->getStanceLegs(planned_contact);       // extracts shouldBeGrounded
    Convert::setContactState(planned_contact, *legs_);  // sets limb strategy naively from planned_contact
  }

  if (publishMeasuredContact_) {
    auto measuredContacts = Convert::getStanceLegs(*legs_);
    gaitPattern_->setStanceLegs(measuredContacts);
  }

  // Publish Observation
  //    publishObservation();

  mpcTime_ += dt;

  return true;
}

bool TrackingCtrlImplicitContactOpt::toggleJoystickServiceCallback(std_srvs::SetBool::Request& request,
                                                                   std_srvs::SetBool::Response& response) {
  useJoystick_ = request.data;
  response.success = 1;
  return true;
}

bool TrackingCtrlImplicitContactOpt::workerCallback(const roco::WorkerEvent& event) const {
  visualizeStateInRviz();
  return true;
}

void TrackingCtrlImplicitContactOpt::visualizeStateInRviz() const {
  desiredStateVisualizer_.update(*wholeBody_, ros::Time::now());
  desiredStateVisualizer_.publish();

  torsoVisualizer_.update(wholeBody_->getTorso(), wholeBody_->getLegs(), *terrainModel_);
  torsoVisualizer_.publish();

  feetVisualizer_.update(wholeBody_->getLegs());
  feetVisualizer_.publish();

  contactForceVisualizer_.update(wholeBody_->getLimbs());
  contactForceVisualizer_.publish();
}

void TrackingCtrlImplicitContactOpt::trackInitialState() {
  // TODO: just freeze
}

bool TrackingCtrlImplicitContactOpt::preStop() {
  cancelWorker(publishWorkerHandle_, true);

  // visualization
  desiredStateVisualizer_.shutdown();
  feetVisualizer_.shutdown();
  torsoVisualizer_.shutdown();
  contactForceVisualizer_.shutdown();
  toggleJoystickServiceServer_.shutdown();
  return true;
}

TiXmlHandle TrackingCtrlImplicitContactOpt::loadXMLCommonConfig() {
  // static so returned xml handle can still be used after function returns.
  static loco::ParameterSet parameterSet;
  std::string parameterFile = getParameterPath() + "/CommonParameters.xml";

  if (!parameterSet.loadXmlDocument(parameterFile)) MELO_ERROR_STREAM("Could not load parameter file: " << parameterFile);

  return parameterSet.getHandle().FirstChild("ocs2_common");
}

void TrackingCtrlImplicitContactOpt::filterAcceleration() {
  // Base linear
  baseLinearFilter_->advance(torso_->getDesiredStatePtr()->getLinearAccelerationTargetInControlFrame());
  torso_->getDesiredStatePtr()->setLinearAccelerationTargetInControlFrame(baseLinearFilter_->getFilteredValue());

  // Base angular
  baseAngularFilter_->advance(torso_->getDesiredStatePtr()->getAngularAccelerationTargetInControlFrame());
  torso_->getDesiredStatePtr()->setAngularAccelerationTargetInControlFrame(baseAngularFilter_->getFilteredValue());

  // Legs linear
  for (const auto& leg : *legs_) {
    auto id = leg->getId();
    loco::LinearAcceleration legAcceleration = leg->getFoot().getStateDesired().getLinearAccelerationEndEffectorInWorldFrame();
    legAcceleration *= std::min(1.0, (footMaxAccelerationInG_ * 9.81) / legAcceleration.norm());
    legFilters_[id]->advance(legAcceleration);
    leg->getFootPtr()->getStateDesiredPtr()->setLinearAccelerationEndEffectorInWorldFrame(legFilters_[id]->getFilteredValue());
  }
}

} /* namespace anymal_implicit_contact_opt_ctrl */

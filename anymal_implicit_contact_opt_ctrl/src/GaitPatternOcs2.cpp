//
// Created by ruben on 16.08.18.
//

#include "anymal_implicit_contact_opt_ctrl/GaitPatternOcs2.hpp"

namespace loco {
    GaitPatternOcs2::GaitPatternOcs2(Legs& legs) : legs_(legs) {
    }

    double GaitPatternOcs2::getStrideDuration() const {
      return 0;
    }

    void GaitPatternOcs2::setStrideDuration(double strideDuration) {

    }

    double GaitPatternOcs2::getSwingPhaseForLeg(int iLeg) const {
      return 0;
    }

    double GaitPatternOcs2::getSwingPhaseForLeg(int iLeg, double stridePhase) const {
      return 0;
    }

    double GaitPatternOcs2::getStancePhaseForLeg(int iLeg) const {
      return 0;
    }

    double GaitPatternOcs2::getStancePhaseForLeg(int iLeg, double stridePhase) const {
      return 0;
    }

    double GaitPatternOcs2::getSwingDuration(int iLeg, double strideDuration) const {
      return 0;
    }

    double GaitPatternOcs2::getStanceDuration(int iLeg, double strideDuration) const {
      return 0;
    }

    double GaitPatternOcs2::getStanceDuration(int iLeg) const {
      return 0;
    }

    unsigned long GaitPatternOcs2::getNGaitCycles() const {
      return 0;
    }

    int GaitPatternOcs2::getNumberOfStanceLegs(double stridePhase) const {
      return 0;
    }

    double GaitPatternOcs2::getTimeLeftInStance(int iLeg, double strideDuration, double stridePhase) const {
      return 0;
    }

    double GaitPatternOcs2::getTimeLeftInSwing(int iLeg, double strideDuration, double stridePhase) const {
      return 0;
    }

    double GaitPatternOcs2::getTimeSpentInStance(int iLeg, double strideDuration, double stridePhase) const {
      return 0;
    }

    double GaitPatternOcs2::getTimeSpentInSwing(int iLeg, double strideDuration, double stridePhase) const {
      return 0;
    }

    double GaitPatternOcs2::getTimeUntilNextStancePhase(int iLeg, double strideDuration, double stridePhase) const {
      return 0;
    }

    double GaitPatternOcs2::getTimeUntilNextSwingPhase(int iLeg, double strideDuration, double stridePhase) const {
      return 0;
    }

    bool GaitPatternOcs2::addVariablesToLog(bool update) {
      return GaitPatternBase::addVariablesToLog(update);
    }

    bool GaitPatternOcs2::shouldBeLegGrounded(int iLeg) const {
      return false;
    }

    double GaitPatternOcs2::getStridePhase() const {
      return 0;
    }

    void GaitPatternOcs2::setStridePhase(double stridePhase) {

    }

    bool GaitPatternOcs2::setToInterpolated(const GaitPatternBase &gaitPattern1, const GaitPatternBase &gaitPattern2,
                                            double t) {
      return GaitPatternBase::setToInterpolated(gaitPattern1, gaitPattern2, t);
    }

    void GaitPatternOcs2::setSwingPhaseProgress(int iLeg, double stridePhase) {
      swingPhaseProgress_[iLeg] = stridePhase;
    }

    void GaitPatternOcs2::setStanceLegs(std::array<bool, 4>& contact_flags) {
      contact_flags_ = contact_flags;
    }

    void GaitPatternOcs2::getStanceLegs(std::array<bool, 4>& contact_flags) {
       contact_flags = contact_flags_;
    }

    bool GaitPatternOcs2::advance(double dt) {
      for (auto leg : legs_) {
        const auto iLeg = leg->getId();

        //--- defined by the "planning" / timing
        leg->getContactSchedulePtr()->setShouldBeGrounded(contact_flags_[iLeg]);
        leg->getContactSchedulePtr()->setSwingPhase(swingPhaseProgress_[iLeg]);
      }
    }

    bool GaitPatternOcs2::initialize(double dt) {
      swingPhaseProgress_.fill(0.0);
      contact_flags_.fill(true);
    }

} // namespace loco

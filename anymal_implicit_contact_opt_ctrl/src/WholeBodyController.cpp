//
// Created by ruben on 26.08.18.
//

#include <anymal_implicit_contact_opt_ctrl/WholeBodyController.hpp>

namespace anymal_implicit_contact_opt_ctrl {

    WholeBodyControllerOcs2::WholeBodyControllerOcs2(loco::WholeBody &wholeBody,
                                                     const RobotModel &model,
                                                     loco::TerrainModelBase &terrainModel) :
    Base(wholeBody, model, terrainModel) {};

    bool WholeBodyControllerOcs2::addOptimizationTasks()
    {
      if(!Base::addOptimizationTasks()) {return false;};
      if(!addForceTrackingTask(2, 0.001)) {return false;};
      return true;
    };

    bool WholeBodyControllerOcs2::addForceTrackingTask(int priority, double weight){
      const auto numContacts = supportJacobian_.getNumberOfContacts();
      Eigen::MatrixXd A = Eigen::MatrixXd::Zero(3*numContacts, solutionSpaceDimension_);
      A.rightCols(3*numContacts).setIdentity();

      // WBC optimizes over negative Lambdas in contact in a frame defined in supportJacobian
      Eigen::VectorXd b(3*numContacts);

      int nextLegIdx = 0;
      for (const auto contactKey : RD::getContactKeys()) {
        const auto contactEnum = contactKey.getEnum();
        const auto branchEnum = RD::template mapEnums<BranchEnum>(contactEnum);
        const auto limbEnum = RD::template mapEnums<LimbEnum>(branchEnum);

        // Skip this limb if it is not in contact.
        if (contactFlags_[limbEnum] != whole_body_control_romo::ContactStateEnum::ContactClosed3Dof) { continue; }

        const auto limbId = RD::template mapKeyEnumToKeyId<BranchEnum, LimbEnum>(branchEnum);

        const auto &limb = *wholeBody_.getLimbsPtr()->getPtr(limbId);
        // ! Due to loco convention, contactForces_W are the desired forces onto the world -> negative
        const auto contactForce_W = -limb.getEndEffector().getStateDesired().getForceAtEndEffectorInWorldFrame();
        if (supportJacobian_.getForceFrame() == CoordinateFrameEnum::BASE) {
          const auto &ori_WtoB = wholeBody_.getTorso().getMeasuredState().getOrientationWorldToBase();
          const auto contactForces_base = ori_WtoB.rotate(contactForce_W);
          b.segment<3>(nextLegIdx) = contactForces_base.toImplementation();
        } else if (supportJacobian_.getForceFrame() == CoordinateFrameEnum::WORLD){
          b.segment<3>(nextLegIdx) = contactForce_W.toImplementation();
        } else {
          throw std::runtime_error("[addForceTrackingTask] Support Frame not supported");
        }
        nextLegIdx +=3;
      }

      return hierarchicalOptimization_->addOptimizationProblem(
          "track_contact_forces", priority,
          std::move(A), b,
          Eigen::MatrixXd(),
          Eigen::VectorXd(),
          weight*Eigen::VectorXd::Ones(A.rows()),
          Eigen::VectorXd(),
          hopt::ConstraintType::Equality);
    }





} // namespace anymal_ctrl_track_ocs2_common

#include "foot_unloaded_lift_cost.hpp"

namespace anymal_implicit_contact_opt {

template <size_t STATE_DIM, size_t CONTROL_DIM, typename SCALAR_EVAL, typename SCALAR>
FootUnloadedLiftCost<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>::FootUnloadedLiftCost() : Base("foot_unloaded_lift_cost") {
  Q_.setZero();
  R_.setZero();
  x_ref_.setZero();
  u_ref_.setZero();
}

template <size_t STATE_DIM, size_t CONTROL_DIM, typename SCALAR_EVAL, typename SCALAR>
SCALAR FootUnloadedLiftCost<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>::evaluate(const state_vector_t& x, const control_vector_t& u,
                                                                                   const SCALAR& t) {
  return evalLocal<SCALAR>(x, u, t);
}

template <size_t STATE_DIM, size_t CONTROL_DIM, typename SCALAR_EVAL, typename SCALAR>
ct::core::ADCGScalar FootUnloadedLiftCost<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>::evaluateCppadCg(
    const ct::core::StateVector<STATE_DIM, ct::core::ADCGScalar>& x, const ct::core::ControlVector<CONTROL_DIM, ct::core::ADCGScalar>& u,
    ct::core::ADCGScalar t) {
  return evalLocal<ct::core::ADCGScalar>(x, u, t);
}

template <size_t STATE_DIM, size_t CONTROL_DIM, typename SCALAR_EVAL, typename SCALAR>
FootUnloadedLiftCost<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>*
FootUnloadedLiftCost<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>::clone() const {
  return new FootUnloadedLiftCost(*this);
}

template <size_t STATE_DIM, size_t CONTROL_DIM, typename SCALAR_EVAL, typename SCALAR>
void FootUnloadedLiftCost<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>::loadConfigFile(const std::string& filename,
                                                                                       const std::string& termName, bool verbose) {
  ct::optcon::loadMatrixCF(filename, "Q", Q_, termName);
  ct::optcon::loadMatrixCF(filename, "R", R_, termName);
  ct::optcon::loadMatrixCF(filename, "x_des", x_ref_, termName);
  ct::optcon::loadMatrixCF(filename, "u_des", u_ref_, termName);
  if (verbose) {
    std::cout << "Reading " << termName << std::endl;
    std::cout << "Read Q as Q = \n" << Q_ << std::endl;
    std::cout << "Read R as R = \n" << R_ << std::endl;
    std::cout << "Read x_ref as x_ref = \n" << x_ref_.transpose() << std::endl;
    std::cout << "Read u_ref as u_ref = \n" << u_ref_.transpose() << std::endl;
  }
}

}  // namespace anymal_implicit_contact_opt

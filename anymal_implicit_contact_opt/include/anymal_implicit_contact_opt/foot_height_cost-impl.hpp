#include "foot_height_cost.hpp"

#include <anymal_bear_ct_model/ANYmalWithContact.h>

namespace anymal_implicit_contact_opt {

template <size_t STATE_DIM, size_t CONTROL_DIM, typename SCALAR_EVAL, typename SCALAR>
std::mutex FootHeightCost<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>::cppADCompileMutex_;

template <size_t STATE_DIM, size_t CONTROL_DIM, typename SCALAR_EVAL, typename SCALAR>
FootHeightCost<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>::FootHeightCost()
    : FootHeightCost(SCALAR(1.0), Eigen::Matrix<SCALAR, 4, 12>::Zero()) {}

template <size_t STATE_DIM, size_t CONTROL_DIM, typename SCALAR_EVAL, typename SCALAR>
FootHeightCost<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>::FootHeightCost(SCALAR scaling, const Eigen::Matrix<SCALAR, 4, 12>& policy)
    : Base("foot_height_cost"), scaling_(scaling), K_(policy) {
  typename derivativesCppadJIT::FUN_TYPE_CG f = [this](const Eigen::Matrix<CG_SCALAR, STATE_DIM + CONTROL_DIM, 1>& in) {
    return Eigen::Matrix<CG_SCALAR, 1, 1>(
        this->evalLocal<CG_SCALAR>(in.template head<STATE_DIM>(), in.template tail<CONTROL_DIM>(), CG_SCALAR(0.0)));
  };

  std::cout << "waiting for lock" << std::endl;
  std::lock_guard<std::mutex> lock(FootHeightCost<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>::cppADCompileMutex_);
  std::cout << "acquired lock" << std::endl;
  jit_derivatives_.reset(new derivativesCppadJIT(f));

  x_cached_.setRandom();
  u_cached_.setRandom();

  std::cout << "Scaling for foot height cost: ";
  std::cin >> scaling_;

  compileJit();
}

template <size_t STATE_DIM, size_t CONTROL_DIM, typename SCALAR_EVAL, typename SCALAR>
SCALAR FootHeightCost<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>::evaluate(const state_vector_t& x, const control_vector_t& u,
                                                                             const SCALAR& t) {
  return evalLocal<SCALAR>(x, u, t);
}

template <size_t STATE_DIM, size_t CONTROL_DIM, typename SCALAR_EVAL, typename SCALAR>
ct::core::ADCGScalar FootHeightCost<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>::evaluateCppadCg(
    const ct::core::StateVector<STATE_DIM, ct::core::ADCGScalar>& x, const ct::core::ControlVector<CONTROL_DIM, ct::core::ADCGScalar>& u,
    ct::core::ADCGScalar t) {
  return evalLocal<ct::core::ADCGScalar>(x, u, t);
}

template <size_t STATE_DIM, size_t CONTROL_DIM, typename SCALAR_EVAL, typename SCALAR>
FootHeightCost<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>* FootHeightCost<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>::clone() const {
  return new FootHeightCost(*this);
}

template <size_t STATE_DIM, size_t CONTROL_DIM, typename SCALAR_EVAL, typename SCALAR>
void FootHeightCost<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>::loadConfigFile(const std::string& filename, const std::string& termName,
                                                                                 bool verbose) {
  boost::property_tree::ptree pt;
  try {
    boost::property_tree::read_info(filename, pt);
  } catch (...) {
    std::cout << "Error when loading foot_height_cost from config file " << filename << std::endl;
    return;
  }
  this->name_ = pt.get<std::string>(termName + ".name", termName);
  this->scaling_ = SCALAR_EVAL(pt.get<double>(termName + ".scaling", 1.0));
}

template <size_t STATE_DIM, size_t CONTROL_DIM, typename SCALAR_EVAL, typename SCALAR>
ct::core::StateVector<STATE_DIM, SCALAR_EVAL> FootHeightCost<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>::stateDerivative(
    const ct::core::StateVector<STATE_DIM, SCALAR_EVAL>& x, const ct::core::ControlVector<CONTROL_DIM, SCALAR_EVAL>& u,
    const SCALAR_EVAL& t) {
  evalDerivatives(x, u);
  return jac_.template head<STATE_DIM>();
}

template <size_t STATE_DIM, size_t CONTROL_DIM, typename SCALAR_EVAL, typename SCALAR>
ct::core::ControlVector<CONTROL_DIM, SCALAR_EVAL> FootHeightCost<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>::controlDerivative(
    const ct::core::StateVector<STATE_DIM, SCALAR_EVAL>& x, const ct::core::ControlVector<CONTROL_DIM, SCALAR_EVAL>& u,
    const SCALAR_EVAL& t) {
  return ct::core::ControlVector<CONTROL_DIM, SCALAR_EVAL>::Zero();
}

template <size_t STATE_DIM, size_t CONTROL_DIM, typename SCALAR_EVAL, typename SCALAR>
typename FootHeightCost<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>::control_state_matrix_t
FootHeightCost<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>::stateControlDerivative(
    const ct::core::StateVector<STATE_DIM, SCALAR_EVAL>& x, const ct::core::ControlVector<CONTROL_DIM, SCALAR_EVAL>& u,
    const SCALAR_EVAL& t) {
  return FootHeightCost<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>::control_state_matrix_t::Zero();
}

template <size_t STATE_DIM, size_t CONTROL_DIM, typename SCALAR_EVAL, typename SCALAR>
typename FootHeightCost<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>::state_matrix_t
FootHeightCost<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>::stateSecondDerivative(
    const ct::core::StateVector<STATE_DIM, SCALAR_EVAL>& x, const ct::core::ControlVector<CONTROL_DIM, SCALAR_EVAL>& u,
    const SCALAR_EVAL& t) {
  evalDerivatives(x, u);
  return hes_.template block<STATE_DIM, STATE_DIM>(0, 0);
}

template <size_t STATE_DIM, size_t CONTROL_DIM, typename SCALAR_EVAL, typename SCALAR>
typename FootHeightCost<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>::control_matrix_t
FootHeightCost<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>::controlSecondDerivative(
    const ct::core::StateVector<STATE_DIM, SCALAR_EVAL>& x, const ct::core::ControlVector<CONTROL_DIM, SCALAR_EVAL>& u,
    const SCALAR_EVAL& t) {
  return FootHeightCost<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>::control_matrix_t::Zero();
}

template <size_t STATE_DIM, size_t CONTROL_DIM, typename SCALAR_EVAL, typename SCALAR>
void FootHeightCost<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>::compileJit() {
  ct::core::DerivativesCppadSettings settings;
  settings.createSparseJacobian_ = false;
  settings.createJacobian_ = true;
  settings.createSparseHessian_ = false;
  settings.createHessian_ = true;
  settings.multiThreading_ = true;

  // compile the Jacobian
  std::cout << "compileJIT footHeightCost..." << std::endl;
  jit_derivatives_->compileJIT(settings, "footHeightCostCgLib");
  std::cout << "compileJIT done" << std::endl;
}

template <size_t STATE_DIM, size_t CONTROL_DIM, typename SCALAR_EVAL, typename SCALAR>
void FootHeightCost<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>::evalDerivatives(
    const ct::core::StateVector<STATE_DIM, SCALAR_EVAL>& x, const ct::core::ControlVector<CONTROL_DIM, SCALAR_EVAL>& u) {
  if (u == u_cached_ && x == x_cached_) {
    return;
  }
  Eigen::Matrix<double, STATE_DIM + CONTROL_DIM, 1> x_in;
  x_in << x, u;

  Eigen::Matrix<double, 1, 1> w(1.0);
  jac_ = jit_derivatives_->jacobian(x_in);    // TODO: sparse jacobian
  hes_ = jit_derivatives_->hessian(x_in, w);  // TODO: sparse hessian

  u_cached_ = u;
  x_cached_ = x;
}

template <size_t STATE_DIM, size_t CONTROL_DIM, typename SCALAR_EVAL, typename SCALAR>
double FootHeightCost<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>::footHoldActivation(const Eigen::Vector2d& footPosInPlane) {
  std::vector<Eigen::Vector2d> footHolds;
  footHolds.push_back(Eigen::Vector2d(0.44 - 0.0, 0.25));
  footHolds.push_back(Eigen::Vector2d(0.44 - 0.3, 0.25));
  footHolds.push_back(Eigen::Vector2d(0.44 - 0.6, 0.25));
  footHolds.push_back(Eigen::Vector2d(0.44 - 0.9, 0.25));
  footHolds.push_back(Eigen::Vector2d(0.44 - 1.2, 0.25));

  footHolds.push_back(Eigen::Vector2d(0.44 - 0.0, -0.25));
  footHolds.push_back(Eigen::Vector2d(0.44 - 0.3, -0.25));
  footHolds.push_back(Eigen::Vector2d(0.44 - 0.6, -0.25));
  footHolds.push_back(Eigen::Vector2d(0.44 - 0.9, -0.25));
  footHolds.push_back(Eigen::Vector2d(0.44 - 1.2, -0.25));

  // TODO implement smarter way to find closest foothold
  auto closestFoothold =
      std::min(footHolds.begin(), footHolds.end(), [&footPosInPlane](const Eigen::Vector2d& a, const Eigen::Vector2d& b) {
        return (footPosInPlane - a).squaredNorm() < (footPosInPlane - b).squaredNorm();
      });

  constexpr double amp1 = 1.0;
  constexpr double amp2 = 200.0;

  const double tanh_eval = std::tanh(amp2 * (footPosInPlane - closestFoothold).squaredNorm());
  double activation = amp1 * tanh_eval;

  Eigen::Vector2d dActivation_dx = amp1 * (1.0 - std::pow(tanh_eval, 2)) * amp2 * 2.0 * (footPosInPlane - closestFoothold);

  Eigen::Matrix2d ddActivation_dxx = amp1 * (-2.0 * tanh_eval * amp2 * 2.0 * (footPosInPlane - closestFoothold)) * amp2 * 2.0 *
                                         (footPosInPlane - closestFoothold).transpose() +
                                     amp1 * (1.0 - std::pow(tanh_eval, 2)) * amp2 * 2.0 * Eigen::Matrix2d::Identity();
}

}  // namespace anymal_implicit_contact_opt

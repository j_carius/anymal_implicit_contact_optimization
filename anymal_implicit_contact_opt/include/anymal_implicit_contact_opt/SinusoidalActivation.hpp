#pragma once

#include <ct/core/common/activations/ActivationBase.hpp>

namespace anymal_implicit_contact_opt {

/*!
 * Sinusoidal activation for peridic cost terms
 * activation = amplitude * sin(freq*t + phase) + offset
 */
template <typename SCALAR = double, typename TRAIT = typename ct::core::tpl::TraitSelector<SCALAR>::Trait>
class SinusoidalActivation : public ct::core::tpl::ActivationBase<SCALAR> {
 public:
  SinusoidalActivation() = default;
  SinusoidalActivation(const SCALAR amplitude, const SCALAR frequency, const SCALAR phase, const SCALAR offset)
      : amp_(amplitude), freq_(frequency), phase_(phase), offset_(offset) {}

  virtual bool isActive(const SCALAR t) override final { return true; }

  virtual SCALAR computeActivation(const SCALAR t) override final { return std::max(0.0, amp_ * TRAIT::cos(freq_ * t + phase_) + offset_); }

 private:
  SCALAR amp_;     //! amplitude
  SCALAR freq_;    //! frequency (in rad/sek)
  SCALAR phase_;   //! phase shift (in rad)
  SCALAR offset_;  //! offset (constant term)
};

}  // namespace anymal_implicit_contact_opt

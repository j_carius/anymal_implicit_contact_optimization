#pragma once

#include <sstream>

#include <anymal_implicit_contact_opt/anymal_implicit_contact_opt.hpp>
#include <anymal_implicit_contact_opt/foot_height_cost.hpp>
#include "anymal_implicit_contact_opt/SinusoidalActivation.hpp"

namespace anymal_implicit_contact_opt {

using anymalOpt_t = anymal_implicit_contact_opt::ANYmalImplicitContactOpt;
const size_t STATE_DIM = anymalOpt_t::STATE_DIM;
const size_t CONTROL_DIM = anymalOpt_t::CONTROL_DIM;
using termQ_t = ct::optcon::TermQuadratic<STATE_DIM, CONTROL_DIM>;
using termFH_t = anymal_implicit_contact_opt::FootHeightCost<STATE_DIM, CONTROL_DIM>;
using termFH_p_t = std::shared_ptr<termFH_t>;
using termFH_vec_t = std::vector<termFH_p_t>;
using termFH_vec_vec_t = std::vector<std::shared_ptr<termFH_vec_t>>;

double kernel(double x) { return -1.0 / (std::exp(x) + 2.0 + std::exp(-x)); }

double eval_params(const Eigen::Matrix<double, 4 * 12, 1> params, std::shared_ptr<termFH_vec_t> termFH_vec,
                   std::shared_ptr<anymalOpt_t> anymalOpt = nullptr) {
  if (anymalOpt == nullptr) {
    anymalOpt.reset(new anymalOpt_t());  // put it on the heap
  }
  anymalOpt->initialize();
  auto cf = anymalOpt->getCostFunction();

  Eigen::Matrix<double, 4, 12> K;
  K.row(0) = params.segment<12>(0).transpose();
  K.row(1) = params.segment<12>(12 * 1).transpose();
  K.row(2) = params.segment<12>(12 * 2).transpose();
  K.row(3) = params.segment<12>(12 * 3).transpose();

  constexpr double scale = 5000000.0;
  termFH_p_t termFh(new termFH_t(scale, K));

  Eigen::Vector3d zero3d;
  zero3d.setZero();

  cf->addIntermediateTerm(termFh);

  cf->initialize();

  anymalOpt->changeCostFunction(cf);
  anymalOpt->solve();

  anymalOpt_t::Solution_t solution;
  anymalOpt->getSolution(solution);

  ct::core::DiscreteArray<typename anymalOpt_t::percussion_t> percussions;
  anymalOpt->getPercussions(percussions);

  //////////////////////////////////////////////////////////////////////////////
  // evaluating the cost (Jemin)
  //////////////////////////////////////////////////////////////////////////////
  constexpr double c_omega1 = -1.0;
  constexpr double c_omega2 = 4.5;
  constexpr double c_v1 = -10.0;
  constexpr double c_v2 = 4.5;
  constexpr double c_tau = 5.0e-2;
  constexpr double jointSpeedMax = 10.0;
  constexpr double c_js = 0.3e-2;
  constexpr double c_f = 40.0;
  constexpr double c_fv = 5.0;
  constexpr double c_o = 0.5;
  constexpr double c_s = 0.2;
  constexpr double c_d = 4000.0;

  Eigen::Vector3d ref_vel;
  ref_vel << 0.5, 0.0, 0.0;

  double cost_omega = 0;
  double cost_v = 0;
  double cost_tau = 0;
  double cost_j = 0;
  double cost_fh = 0;
  double cost_fv = 0;
  double cost_o = 0;
  double cost_s = 0;

  for (int t = 0; t < solution.uff().size(); t++) {
    cost_omega += c_omega1 * kernel(c_omega2 * solution.x_ref()[t].segment<3>(21).norm());  // angular vel
    cost_v += c_v1 * kernel(c_v2 * (solution.x_ref()[t].segment<3>(18) - ref_vel).norm());  // linear vel
    cost_tau += c_tau * solution.uff()[t].norm();                                           // torque cost
    for (int j = 0; j < solution.uff()[t].rows(); j++) {
      if (std::abs(solution.uff()[t](j)) > jointSpeedMax) {
        cost_j += c_js * std::pow(solution.uff()[t](j) - jointSpeedMax, 2);  // joint speed cost
      }
    }
    const auto footHeights = anymalOpt->ANYmalWithContactD_->footHeight(solution.x_ref()[t]);
    const auto footVelocities = anymalOpt->ANYmalWithContactD_->linearFootVelocities(solution.x_ref()[t]);
    for (int foot = 0; foot < footHeights.rows(); foot++) {
      if (percussions[t](3 * foot + 2) < (10.0 * 0.01)) {  // z component of force, 10N threshold
        // clearance cost
        cost_fh += c_f * std::pow(footHeights(foot) - 0.07, 2) * footVelocities.segment<2>(3 * foot).norm();
      } else {
        // slip cost
        cost_fv += c_fv * footVelocities.segment<2>(3 * foot).norm();
      }
    }

    cost_o += c_o * solution.x_ref()[t].segment<3>(3).norm();  // orientation cost
    if (t + 1 < solution.uff().size()) {
      cost_s += c_s * (solution.uff()[t] - solution.uff()[t + 1]).norm();  // smootheness cost
    }
  }
  double cost_d = c_d * std::pow(solution.x_ref().back()(0), 2);  // x distance to goal cost

  const double cost = cost_omega + cost_v + cost_tau + cost_j + cost_fh + cost_fv + cost_o + cost_s + cost_d;

  std::stringstream debugPrint;
  debugPrint << "[eval_params] " << params.transpose() << ". Distance " << solution.x_ref().back()(0) << " cost " << cost << std::endl;
  debugPrint << "[cost split] omega " << cost_omega << " v " << cost_v << " tau " << cost_tau << " j " << cost_j << " fh " << cost_fh
             << " fv " << cost_fv << " o " << cost_o << " s " << cost_s << " d " << cost_d << std::endl;
  std::cout << debugPrint.str();

  return -cost;
}

}  // namespace anymal_implicit_contact_opt

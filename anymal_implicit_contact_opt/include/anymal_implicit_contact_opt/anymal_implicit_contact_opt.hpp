#pragma once

#include <ct/core/core.h>

#include <anymal_bear_ct_model/ANYmalWithContact.h>
#include <anymal_bear_ct_model/codegen/ANYmalWithContactLinearized.h>

namespace anymal_implicit_contact_opt {

class ANYmalImplicitContactOpt {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  using ANYmalWithContactD_t = ct::models::ANYmal::ANYmalWithContact<double>;
  using ANYmalWithContactLinearizedD_t = ct::models::ANYmal::ANYmalWithContactLinearized;

  static const size_t STATE_DIM = ANYmalWithContactD_t::STATE_DIM;
  static const size_t CONTROL_DIM = ANYmalWithContactD_t::CONTROL_DIM;

  using state_vector_t = ANYmalWithContactD_t::state_vector_t;
  using control_vector_t = ANYmalWithContactD_t::control_vector_t;
  using percussion_t = ANYmalWithContactD_t::percussion_t;

  using OptConProblem_t = ct::optcon::DiscreteOptConProblem<STATE_DIM, CONTROL_DIM>;
  using CostFunction_t = ct::optcon::CostFunctionQuadratic<STATE_DIM, CONTROL_DIM>;
  using NLOptConSolver_t = ct::optcon::NLOptConSolver<STATE_DIM, CONTROL_DIM, 0, 0, double, false>;

  using Solution_t = ct::core::StateFeedbackController<STATE_DIM, CONTROL_DIM>;

  // using DmsSolver_t = ct::optcon::DmsSolver<STATE_DIM, CONTROL_DIM, double, false>;

  //! constructor
  ANYmalImplicitContactOpt() = default;

  //! all necessary setup work. To be called before any other method
  void initialize();

  //! run direct multiple shooting solver
  void solveDMS();

  /**
   * Set complete initialization control. Must be called once before solving
   * the problem
   */
  void setInitialGuess(const state_vector_t& x0);

  /**
   * Change the initial state. Should be called in an MPC implementation
   * for updating with the currently measured state.
   */
  void changeInitialState(const state_vector_t& x0);

  //! solve the NLOC problem. Returns true if sucsessful
  bool solve() { return solver_->solve(); }

  /**
   * run a single iteration of the solver (might not be supported by all solvers)
   * @return true if a better solution was found
   */
  bool runIteration();

  /**
   * run timeSteps number of steps in mpc mode (shifts goal forward)
   */
  void runMPC(const int timeSteps);

  void getSolution(Solution_t& solution) const { solution = solver_->getSolution(); }

  double getTimeDiscretization() { return ANYmalWithContactD_->getTimeDiscretization(); }

  double getCost() const { return solver_->getCost(); }

  /**
   * recover the percussions for the current solution
   */
  void getPercussions(ct::core::DiscreteArray<percussion_t>& percussions) const;

  void updateContinuation();

  void changeCostFunction(const std::shared_ptr<CostFunction_t>& cf) { solver_->changeCostFunction(cf); }
  std::shared_ptr<CostFunction_t> getCostFunction() { return costFunction_; }

  // std::shared_ptr<DmsSolver_t> dmsSolver_;

 public:
  std::shared_ptr<ANYmalWithContactD_t> ANYmalWithContactD_;  //! pointer to nonlinear system

 protected:
  std::shared_ptr<ANYmalWithContactLinearizedD_t> ANYmalWithContactLinearizedD_;  //! pointer to linear system

  OptConProblem_t optconProblem_;
  ct::optcon::NLOptConSettings optconSettings_;
  int nSteps_;  //! the number of steps in the optimization problem

  std::shared_ptr<CostFunction_t> costFunction_;

  std::shared_ptr<NLOptConSolver_t> solver_;

  //! initialization quantities
  ct::core::FeedbackArray<STATE_DIM, CONTROL_DIM> K_init_array_;    //! initial array of feedback gains
  ct::core::ControlVectorArray<CONTROL_DIM, double> u_init_array_;  //! initial array of feedforward controls
  state_vector_t x_f_;                                              //! desired final state
};

}  // namespace anymal_implicit_contact_opt

#pragma once

#include <ct/core/Types>
#include <ct/optcon/costfunction/term/TermBase.hpp>

#include <anymal_bear_ct_model/AnymalWithContact-dynamics-dummy.h>

#include <mutex>

namespace anymal_implicit_contact_opt {

template <size_t STATE_DIM, size_t CONTROL_DIM, typename SCALAR_EVAL = double, typename SCALAR = SCALAR_EVAL>
class FootHeightCost : public ct::optcon::TermBase<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR> {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  using state_vector_t = Eigen::Matrix<SCALAR, STATE_DIM, 1>;
  using control_vector_t = Eigen::Matrix<SCALAR, CONTROL_DIM, 1>;
  using Base = ct::optcon::TermBase<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>;
  using state_matrix_t = typename Base::state_matrix_t;
  using control_matrix_t = typename Base::control_matrix_t;
  using control_state_matrix_t = typename Base::control_state_matrix_t;

  using derivativesCppadJIT = ct::core::DerivativesCppadJIT<STATE_DIM + CONTROL_DIM, 1>;
  using CG_SCALAR = typename derivativesCppadJIT::CG_SCALAR;

  FootHeightCost();
  FootHeightCost(SCALAR scaling, const Eigen::Matrix<SCALAR, 4, 12>& policy);

  virtual ~FootHeightCost() = default;

  //! deep copy
  virtual FootHeightCost<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>* clone() const override;

  virtual SCALAR evaluate(const state_vector_t& x, const control_vector_t& u, const SCALAR& t) override final;

  virtual ct::core::ADCGScalar evaluateCppadCg(const ct::core::StateVector<STATE_DIM, ct::core::ADCGScalar>& x,
                                               const ct::core::ControlVector<CONTROL_DIM, ct::core::ADCGScalar>& u,
                                               ct::core::ADCGScalar t) override final;

  virtual ct::core::StateVector<STATE_DIM, SCALAR_EVAL> stateDerivative(const ct::core::StateVector<STATE_DIM, SCALAR_EVAL>& x,
                                                                        const ct::core::ControlVector<CONTROL_DIM, SCALAR_EVAL>& u,
                                                                        const SCALAR_EVAL& t) override final;

  virtual state_matrix_t stateSecondDerivative(const ct::core::StateVector<STATE_DIM, SCALAR_EVAL>& x,
                                               const ct::core::ControlVector<CONTROL_DIM, SCALAR_EVAL>& u,
                                               const SCALAR_EVAL& t) override final;

  //! compute derivative of this cost term w.r.t. the control input
  virtual ct::core::ControlVector<CONTROL_DIM, SCALAR_EVAL> controlDerivative(const ct::core::StateVector<STATE_DIM, SCALAR_EVAL>& x,
                                                                              const ct::core::ControlVector<CONTROL_DIM, SCALAR_EVAL>& u,
                                                                              const SCALAR_EVAL& t) override final;

  //! compute second order derivative of this cost term w.r.t. the control input
  virtual control_matrix_t controlSecondDerivative(const ct::core::StateVector<STATE_DIM, SCALAR_EVAL>& x,
                                                   const ct::core::ControlVector<CONTROL_DIM, SCALAR_EVAL>& u,
                                                   const SCALAR_EVAL& t) override final;

  //! compute the cross-term derivative (state-control) of this cost function term
  virtual control_state_matrix_t stateControlDerivative(const ct::core::StateVector<STATE_DIM, SCALAR_EVAL>& x,
                                                        const ct::core::ControlVector<CONTROL_DIM, SCALAR_EVAL>& u,
                                                        const SCALAR_EVAL& t) override final;

  virtual void loadConfigFile(const std::string& filename, const std::string& termName, bool verbose = false) override final;

  void compileJit();
  void evalDerivatives(const ct::core::StateVector<STATE_DIM, SCALAR_EVAL>& x, const ct::core::ControlVector<CONTROL_DIM, SCALAR_EVAL>& u);

  double footHoldActivation(const Eigen::Vector2d& footPosInPlane);

 protected:
  template <typename SC>
  SC evalLocal(const Eigen::Matrix<SC, STATE_DIM, 1>& x, const Eigen::Matrix<SC, CONTROL_DIM, 1>& u, const SC& t);

 protected:
  static std::mutex cppADCompileMutex_;  // must protect all instances of this class

  std::shared_ptr<derivativesCppadJIT> jit_derivatives_;

  ct::core::StateVector<STATE_DIM, SCALAR_EVAL> x_cached_;
  ct::core::ControlVector<CONTROL_DIM, SCALAR_EVAL> u_cached_;

  Eigen::Matrix<double, STATE_DIM + CONTROL_DIM, 1> jac_;
  Eigen::Matrix<double, STATE_DIM + CONTROL_DIM, STATE_DIM + CONTROL_DIM> hes_;

  SCALAR_EVAL scaling_;
  Eigen::Matrix<SCALAR_EVAL, 4, 12> K_;
};

template <size_t STATE_DIM, size_t CONTROL_DIM, typename SCALAR_EVAL, typename SCALAR>
template <typename SC>
SC FootHeightCost<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>::evalLocal(const Eigen::Matrix<SC, STATE_DIM, 1>& x,
                                                                          const Eigen::Matrix<SC, CONTROL_DIM, 1>& u, const SC& t) {
  using ANYmalWC = ct::models::ANYmal::ANYmalWithContact<SC>;

  // std::vector<Eigen::Vector2d> footHolds;
  // footHolds.push_back(Eigen::Vector2d(0.45 - 0.0, 0.3));
  // footHolds.push_back(Eigen::Vector2d(0.45 - 0.3, 0.3));
  // footHolds.push_back(Eigen::Vector2d(0.45 - 0.6, 0.3));
  // footHolds.push_back(Eigen::Vector2d(0.45 - 0.9, 0.3));
  // footHolds.push_back(Eigen::Vector2d(0.45 - 1.2, 0.3));
  // footHolds.push_back(Eigen::Vector2d(0.45 - 1.5, 0.3));
  // footHolds.push_back(Eigen::Vector2d(0.45 - 1.8, 0.3));
  //
  // footHolds.push_back(Eigen::Vector2d(0.45 - 0.0, -0.3));
  // footHolds.push_back(Eigen::Vector2d(0.45 - 0.3, -0.3));
  // footHolds.push_back(Eigen::Vector2d(0.45 - 0.6, -0.3));
  // footHolds.push_back(Eigen::Vector2d(0.45 - 0.9, -0.3));
  // footHolds.push_back(Eigen::Vector2d(0.45 - 1.2, -0.3));
  // footHolds.push_back(Eigen::Vector2d(0.45 - 1.5, -0.3));
  // footHolds.push_back(Eigen::Vector2d(0.45 - 1.8, -0.3));

  Eigen::Matrix<SC, 4, 4> T_world_trunk;
  T_world_trunk.setIdentity();
  T_world_trunk.template topLeftCorner<3, 3>() = ANYmalWC::getRotationMatrixTrunkToWorldFromCtState(x);
  T_world_trunk.template topRightCorner<3, 1>() = ANYmalWC::getPositionWorldToTrunkFromCtState(x);

  const auto joint_positions = ANYmalWC::getJointPositionsFromCtState(x);
  iit::ANYmal::tpl::HomogeneousTransforms<typename iit::rbd::tpl::TraitSelector<SC>::Trait> homTransforms;

  const Eigen::Matrix<SC, 4, 4> T_trunk_LF_foot = homTransforms.fr_base_X_fr_LF_FOOT(joint_positions);
  const Eigen::Matrix<SC, 4, 4> T_trunk_RF_foot = homTransforms.fr_base_X_fr_RF_FOOT(joint_positions);
  const Eigen::Matrix<SC, 4, 4> T_trunk_LH_foot = homTransforms.fr_base_X_fr_LH_FOOT(joint_positions);
  const Eigen::Matrix<SC, 4, 4> T_trunk_RH_foot = homTransforms.fr_base_X_fr_RH_FOOT(joint_positions);

  const Eigen::Matrix<SC, 4, 4> T_world_LF_foot = T_world_trunk * T_trunk_LF_foot;
  const Eigen::Matrix<SC, 4, 4> T_world_RF_foot = T_world_trunk * T_trunk_RF_foot;
  const Eigen::Matrix<SC, 4, 4> T_world_LH_foot = T_world_trunk * T_trunk_LH_foot;
  const Eigen::Matrix<SC, 4, 4> T_world_RH_foot = T_world_trunk * T_trunk_RH_foot;

  SC cost(0.0);

  ANYmalWC anymal;
  auto footVel = anymal.linearFootVelocities(x);

  cost += footVel.template segment<2>(0).squaredNorm() * CppAD::exp(-100.0 * T_world_LF_foot(2, 3));
  cost += footVel.template segment<2>(3).squaredNorm() * CppAD::exp(-100.0 * T_world_LF_foot(2, 3));
  cost += footVel.template segment<2>(6).squaredNorm() * CppAD::exp(-100.0 * T_world_LF_foot(2, 3));
  cost += footVel.template segment<2>(9).squaredNorm() * CppAD::exp(-100.0 * T_world_LF_foot(2, 3));

  // SC LF_1_over_squarednorm(0.0);
  // SC RF_1_over_squarednorm(0.0);
  // SC LH_1_over_squarednorm(0.0);
  // SC RH_1_over_squarednorm(0.0);
  //
  // for (const auto& footHold : footHolds) {
  //   LF_1_over_squarednorm += SC(1.0) / (T_world_LF_foot.template topRightCorner<2, 1>() - footHold.cast<SC>()).squaredNorm();
  //   RF_1_over_squarednorm += SC(1.0) / (T_world_RF_foot.template topRightCorner<2, 1>() - footHold.cast<SC>()).squaredNorm();
  //   LH_1_over_squarednorm += SC(1.0) / (T_world_LH_foot.template topRightCorner<2, 1>() - footHold.cast<SC>()).squaredNorm();
  //   RH_1_over_squarednorm += SC(1.0) / (T_world_RH_foot.template topRightCorner<2, 1>() - footHold.cast<SC>()).squaredNorm();
  // }
  //
  // cost += CppAD::tanh(20.0 / LF_1_over_squarednorm) * pow(T_world_LF_foot(2, 3) - SC(0.07), 2);
  // cost += CppAD::tanh(20.0 / RF_1_over_squarednorm) * pow(T_world_RF_foot(2, 3) - SC(0.07), 2);
  // cost += CppAD::tanh(20.0 / LH_1_over_squarednorm) * pow(T_world_LH_foot(2, 3) - SC(0.07), 2);
  // cost += CppAD::tanh(20.0 / RH_1_over_squarednorm) * pow(T_world_RH_foot(2, 3) - SC(0.07), 2);

  return scaling_ * cost;
}

}  // namespace anymal_implicit_contact_opt

#include "foot_height_cost-impl.hpp"

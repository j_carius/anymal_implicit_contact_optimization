#pragma once

#include <ct/core/control/StateFeedbackController.h>

namespace anymal_implicit_contact_opt {
class SerializedStateFeedbackController : public ct::core::StateFeedbackController<36, 12, double> {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW
  using Base = ct::core::StateFeedbackController<36, 12, double>;
  using StateTrajectory = ct::core::StateTrajectory<36, double>;
  using ControlTrajectory = ct::core::ControlTrajectory<12, double>;

  SerializedStateFeedbackController() = default;

  SerializedStateFeedbackController(const Base& ctrl) : Base(ctrl) {
    x_ref_vec_ = ctrl.x_ref().toImplementation();
    uff_vec_ = ctrl.uff().toImplementation();
  }

  template <class Archive>
  void save(Archive& archive) const {
    archive(x_ref_vec_, uff_vec_);
  }

  template <class Archive>
  void load(Archive& archive) {
    archive(x_ref_vec_, uff_vec_);

    ct::core::DiscreteArray<Base::state_vector_t> x_ref_array(x_ref_vec_.begin(), x_ref_vec_.end());
    ct::core::DiscreteArray<Base::control_vector_t> uff_array(uff_vec_.begin(), uff_vec_.end());

    x_ref_ = StateTrajectory(x_ref_array, 0.01, 0.0, ct::core::InterpolationType::ZOH);
    uff_ = ControlTrajectory(uff_array, 0.01, 0.0, ct::core::InterpolationType::ZOH);
  }

  std::vector<Base::state_vector_t, Eigen::aligned_allocator<Base::state_vector_t>> x_ref_vec_;
  std::vector<Base::control_vector_t, Eigen::aligned_allocator<Base::control_vector_t>> uff_vec_;
};
}  // namespace anymal_implicit_contact_opt
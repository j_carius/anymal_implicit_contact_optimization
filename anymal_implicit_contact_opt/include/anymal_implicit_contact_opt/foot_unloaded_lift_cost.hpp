#pragma once

#include <ct/optcon/costfunction/term/TermBase.hpp>

namespace anymal_implicit_contact_opt {

template <size_t STATE_DIM, size_t CONTROL_DIM, typename SCALAR_EVAL = double, typename SCALAR = SCALAR_EVAL>
class FootUnloadedLiftCost : public ct::optcon::TermBase<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR> {
 public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW

  using state_vector_t = Eigen::Matrix<SCALAR, STATE_DIM, 1>;
  using control_vector_t = Eigen::Matrix<SCALAR, CONTROL_DIM, 1>;
  using Base = ct::optcon::TermBase<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>;
  using state_matrix_t = typename Base::state_matrix_t;
  using control_matrix_t = typename Base::control_matrix_t;
  using control_state_matrix_t = typename Base::control_state_matrix_t;

  FootUnloadedLiftCost();
  virtual ~FootUnloadedLiftCost() = default;

  //! deep copy
  virtual FootUnloadedLiftCost<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>* clone() const override;

  virtual SCALAR evaluate(const state_vector_t& x, const control_vector_t& u, const SCALAR& t) override;

  virtual ct::core::ADCGScalar evaluateCppadCg(const ct::core::StateVector<STATE_DIM, ct::core::ADCGScalar>& x,
                                               const ct::core::ControlVector<CONTROL_DIM, ct::core::ADCGScalar>& u,
                                               ct::core::ADCGScalar t) override;

  virtual void loadConfigFile(const std::string& filename, const std::string& termName, bool verbose = false) override;

 protected:
  template <typename SC>
  SC evalLocal(const Eigen::Matrix<SC, STATE_DIM, 1>& x, const Eigen::Matrix<SC, CONTROL_DIM, 1>& u, const SC& t);

  state_matrix_t Q_;
  control_matrix_t R_;

  ct::core::StateVector<STATE_DIM, SCALAR_EVAL> x_ref_;
  ct::core::ControlVector<CONTROL_DIM, SCALAR_EVAL> u_ref_;
};

template <size_t STATE_DIM, size_t CONTROL_DIM, typename SCALAR_EVAL, typename SCALAR>
template <typename SC>
SC FootUnloadedLiftCost<STATE_DIM, CONTROL_DIM, SCALAR_EVAL, SCALAR>::evalLocal(const Eigen::Matrix<SC, STATE_DIM, 1>& x,
                                                                                const Eigen::Matrix<SC, CONTROL_DIM, 1>& u, const SC& t) {
  // TODO (jcarius) use kronecker product here

  Eigen::Matrix<SC, STATE_DIM, 1> xDiff = (x - x_ref_.template cast<SC>());
  Eigen::Matrix<SC, CONTROL_DIM, 1> uDiff = (u - u_ref_.template cast<SC>());

  SC activation_LF = CppAD::exp(
      -(uDiff.template segment<3>(0).transpose() * R_.template block<3, 3>(0, 0).template cast<SC>() * uDiff.template segment<3>(0))(0, 0));
  SC activation_RF = CppAD::exp(
      -(uDiff.template segment<3>(3).transpose() * R_.template block<3, 3>(3, 3).template cast<SC>() * uDiff.template segment<3>(3))(0, 0));
  SC activation_LH = CppAD::exp(
      -(uDiff.template segment<3>(6).transpose() * R_.template block<3, 3>(6, 6).template cast<SC>() * uDiff.template segment<3>(6))(0, 0));
  SC activation_RH = CppAD::exp(
      -(uDiff.template segment<3>(9).transpose() * R_.template block<3, 3>(9, 9).template cast<SC>() * uDiff.template segment<3>(9))(0, 0));

  // FIXME (jcarius) this assumes a specific ordering of the state vector
  SC setpointCost_LF =
      (xDiff.template segment<3>(6).transpose() * Q_.template block<3, 3>(6, 6).template cast<SC>() * xDiff.template segment<3>(6))(0, 0);
  SC setpointCost_RF =
      (xDiff.template segment<3>(9).transpose() * Q_.template block<3, 3>(9, 9).template cast<SC>() * xDiff.template segment<3>(9))(0, 0);
  SC setpointCost_LH = (xDiff.template segment<3>(12).transpose() * Q_.template block<3, 3>(12, 12).template cast<SC>() *
                        xDiff.template segment<3>(12))(0, 0);
  SC setpointCost_RH = (xDiff.template segment<3>(15).transpose() * Q_.template block<3, 3>(15, 15).template cast<SC>() *
                        xDiff.template segment<3>(15))(0, 0);

  return activation_LF * setpointCost_LF + activation_RF * setpointCost_RF + activation_LH * setpointCost_LH +
         activation_RH * setpointCost_RH;
}

}  // namespace anymal_implicit_contact_opt

#include "foot_unloaded_lift_cost-impl.hpp"

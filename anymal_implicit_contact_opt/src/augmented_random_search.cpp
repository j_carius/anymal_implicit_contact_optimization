// Augmented Random Search as proposed by https://arxiv.org/pdf/1803.07055.pdf

#include <numeric>

#include <anymal_implicit_contact_opt/ctpl_stl.h>
#include "anymal_implicit_contact_opt/augmented_random_search.hpp"

#include <anymal_implicit_contact_opt/foot_height_cost.hpp>

int main(int argc, char* argv[]) {
  constexpr double alpha = 0.1;   // step size
  constexpr int N = 13;           // number of directions per step
  constexpr int numThreads = 26;  // number of threads to use
  constexpr double nu = 1;        // exploration noise (std.dev)
  constexpr int b = N - 7;        // number of top-performing directions to use for update

  constexpr int param_size = 4 * 12;

  // initialize foot height cost instances
  using anymalOpt_t = anymal_implicit_contact_opt::ANYmalImplicitContactOpt;
  const size_t STATE_DIM = anymalOpt_t::STATE_DIM;
  const size_t CONTROL_DIM = anymalOpt_t::CONTROL_DIM;
  using termFH_t = anymal_implicit_contact_opt::FootHeightCost<STATE_DIM, CONTROL_DIM>;
  using termFH_p_t = std::shared_ptr<termFH_t>;
  using termFH_vec_t = std::vector<termFH_p_t>;
  using termFH_vec_vec_t = std::vector<std::shared_ptr<termFH_vec_t>>;

  using SCALAR = double;
  Eigen::Matrix<SCALAR, 4, 1> scalingZ_LF, scalingZ_RF, scalingZ_LH, scalingZ_RH;
  scalingZ_LF << SCALAR(1.0), SCALAR(0.0), SCALAR(0.0), SCALAR(0.0);
  scalingZ_RF << SCALAR(0.0), SCALAR(1.0), SCALAR(0.0), SCALAR(0.0);
  scalingZ_LH << SCALAR(0.0), SCALAR(0.0), SCALAR(1.0), SCALAR(0.0);
  scalingZ_RH << SCALAR(0.0), SCALAR(0.0), SCALAR(0.0), SCALAR(1.0);

  Eigen::Matrix<SCALAR, 4, 1> zRef;
  zRef.setConstant(SCALAR(0.07));

  termFH_p_t term_LF;
  termFH_p_t term_RF;
  termFH_p_t term_LH;
  termFH_p_t term_RH;

  std::shared_ptr<termFH_vec_vec_t> fhCostContainer(new termFH_vec_vec_t);
  for (int th = 0; th < numThreads; th++) {
    // term_LF.reset(new termFH_t(scalingZ_LF, zRef));
    // term_RF.reset(new termFH_t(scalingZ_RF, zRef));
    // term_LH.reset(new termFH_t(scalingZ_LH, zRef));
    // term_RH.reset(new termFH_t(scalingZ_RH, zRef));

    fhCostContainer->emplace_back(new termFH_vec_t({term_LF, term_RF, term_LH, term_RH}));
  }

  std::default_random_engine generator;
  std::normal_distribution<double> distribution(0.0, 1.0);
  auto normal = [&](double) -> double { return distribution(generator); };  // dummy argument required by Eigen

  ctpl::thread_pool threadPool(numThreads);
  std::vector<std::future<double>> resultsFuture(N * 2);
  using tuple_t = std::tuple<double, double, Eigen::Matrix<double, param_size, 1>>;
  std::vector<tuple_t> results(N);  // used for sorting

  Eigen::Matrix<double, param_size, 1> M;
  M.setZero();

  for (int iter = 0; iter < 500; iter++) {
    std::cout << "++++\n++++\n++++\nITER " << iter << "\n++++\n++++" << std::endl;
    // sample search directions and perform optimization
    std::vector<Eigen::Matrix<double, param_size, 1>> delta(N);
    for (auto& delta_i : delta) {
      delta_i = delta_i.unaryExpr(normal);
    }

    // perform optimizations with perturbed parameter
    for (int i = 0; i < N; i++) {
      resultsFuture[2 * i + 0] = threadPool.push([=](int threadId) {
        return anymal_implicit_contact_opt::eval_params(M + nu * delta[i], (*fhCostContainer)[threadId]);
      });  // int arg requird by lib
      resultsFuture[2 * i + 1] = threadPool.push([=](int threadId) {
        return anymal_implicit_contact_opt::eval_params(M - nu * delta[i], (*fhCostContainer)[threadId]);
      });  // int arg passed by thread pool lib
    }

    // collect results
    for (int i = 0; i < N; i++) {
      results[i] = std::make_tuple(resultsFuture[2 * i + 0].get(), resultsFuture[2 * i + 1].get(), delta[i]);
    }

    std::sort(results.begin(), results.end(),
              [](tuple_t a, tuple_t b) { return std::max(std::get<0>(a), std::get<1>(a)) > std::max(std::get<0>(b), std::get<1>(b)); });

    // calc stddev of 2b rewards
    double res_mean =
        std::accumulate(results.begin(), results.end(), 0.0, [](double a, tuple_t b) { return a + std::get<0>(b) + std::get<1>(b); }) /
        (2.0 * N);
    double accum = 0.0;
    std::for_each(results.begin(), results.end(), [&](tuple_t b) {
      accum += (std::get<0>(b) - res_mean) * (std::get<0>(b) - res_mean) + (std::get<1>(b) - res_mean) * (std::get<1>(b) - res_mean);
    });
    double res_stddev = std::sqrt(accum / (2.0 * N));

    if (res_stddev < 1e-10) {
      std::cout << "res_stddev too small; not performing update." << std::endl;
      continue;
    }

    // update step
    for (int dir = 0; dir < b; dir++) {
      M += alpha / (b * res_stddev) * (std::get<0>(results[dir]) - std::get<1>(results[dir])) * std::get<2>(results[dir]);
    }

    std::cout << "updated M: " << M.transpose() << std::endl;
    if (!M.allFinite()) {
      std::cout << "aborting because M is no longer finite." << std::endl;
      break;
    }
  }
}

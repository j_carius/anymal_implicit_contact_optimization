// $Id$
/* --------------------------------------------------------------------------
CppAD: C++ Algorithmic Differentiation: Copyright (C) 2003-16 Bradley M. Bell

CppAD is distributed under multiple licenses. This distribution is under
the terms of the
                    Eclipse Public License Version 1.0.

A copy of this license is included in the COPYING file of this distribution.
Please visit http://www.coin-or.org/CppAD/ for information on other licenses.
-------------------------------------------------------------------------- */

// documentation what these methods do:
// https://coin-or.github.io/CppAD/doc/atomic_forward.htm,
// https://coin-or.github.io/CppAD/doc/atomic_reverse.htm

/*
$begin atomic_get_started.cpp$$

$section Getting Started with Atomic Operations: Example and Test$$

$head Purpose$$
This example demonstrates the minimal amount of information
necessary for a $cref atomic_base$$ operation.

$nospell

$head Start Class Definition$$
$srccode%cpp% */
#include <cppad/cppad.hpp>
class atomic_get_started : public CppAD::atomic_base<double> {
  /* %$$

  $head Constructor$$
  $srccode%cpp% */
 public:
  // constructor (could use const char* for name)
  atomic_get_started(const std::string& name)
      :  // this example does not use any sparsity patterns
        CppAD::atomic_base<double>(name) {}

 private:
  /* %$$
  $head forward$$
  $srccode%cpp% */
  // p: lowest order taylor coeff that we are evaluating
  // q: highest order taylor coeff that we are evaluating
  // vx, vy: sparsity of tx, ty
  // tx: taylor coefficient tx[j*(q+1)+k]: kth derivative of jth coponent of x [input]
  // ty: taylor coefficient ty[j*(q+1)+k]: kth derivative of jth coponent of y [output]

  // forward mode routine called by CppAD
  virtual bool forward(size_t p, size_t q, const CppAD::vector<bool>& vx, CppAD::vector<bool>& vy, const CppAD::vector<double>& tx,
                       CppAD::vector<double>& ty) {
#ifndef NDEBUG
    size_t n = tx.size() / (q + 1);
    size_t m = ty.size() / (q + 1);
#endif
    assert(n == 2);  // input size
    assert(m == 1);  // output size
    assert(p <= q);

    // return flag
    bool ok = q <= 1;  // we implement order zero and one forward mode at the moment
    if (!ok) return ok;

    // Variable information must always be implemented.
    // y_0 is a variable if and only if x_0 or x_1 is a variable.
    if (vx.size() > 0) vy[0] = vx[0] | vx[1];

    // Order zero forward mode must always be implemented.
    // y^0 = f( x^0 )
    double x_00 = tx[0 * (q + 1) + 0];  // x_0^0 - value of x[0] for zero order mode
    double x_10 = tx[1 * (q + 1) + 0];  // x_1^0 - value of x[1] for zero order mode
    double f = std::tanh(x_00 + x_10);  // f( x^0 ) - implementation of function
    if (p <= 0) ty[0] = f;              // y_0^0
    if (q <= 0) return ok;              // done with zero order forward mode
    assert(vx.size() == 0);

    // Order one forward mode.
    // This case needed if first order forward mode is used.
    // y^1 = f'( x^0 ) x^1
    double x_01 = tx[0 * (q + 1) + 1];                   // x_0^1 value of first derivative of x[0]
    double x_11 = tx[1 * (q + 1) + 1];                   // x_1^1 value of first derivative of x[1]
    double fp_0 = 1.0 - pow(std::tanh(x_00 + x_10), 2);  // partial f w.r.t x_0^0
    double fp_1 = 1.0 - pow(std::tanh(x_00 + x_10), 2);  // partial f w.r.t x_1^0
    if (p <= 1) ty[1] = fp_0 * x_01 + fp_1 * x_11;       // f'( x^0 ) * x^1
    if (q <= 1) return ok;

    // jca (own test at q=2)
    double x_02 = tx[0 * (q + 1) + 2];  // x_0^2 value of second derivative of x[0]
    double x_12 = tx[1 * (q + 1) + 2];  // x_1^2 value of second derivative of x[1]
    double f_dd0 = -2.0 * std::tanh(x_00 + x_10) * (1.0 - pow(std::tanh(x_00 + x_10), 2));
    double f_dd1 = -2.0 * std::tanh(x_00 + x_10) * (1.0 - pow(std::tanh(x_00 + x_10), 2));
    if (p <= 2) ty[2] = f_dd0 * x_01 * x_01 + fp_0 * x_02 + f_dd1 * x_11 * x_11 + fp_1 * x_12;
    if (q <= 2) return ok;

    // Assume we are not using forward mode with order > 1
    assert(!ok);
    return ok;
  }

  virtual bool reverse(size_t q, const CppAD::vector<double>& tx, const CppAD::vector<double>& ty, CppAD::vector<double>& px,
                       const CppAD::vector<double>& py) {
#ifndef NDEBUG
    size_t n = tx.size() / (q + 1);
    size_t m = ty.size() / (q + 1);
#endif
    assert(px.size() == n * (q + 1));
    assert(py.size() == m * (q + 1));
    assert(n == 2);
    assert(m == 1);
    bool ok = q <= 1;
    if (!ok) return ok;

    double fp_0, fp_1;

    // note: when q=0, we are calculating first derivatives (first order reverse mode)

    // This case needed if first order reverse mode is used
    // F ( {x} ) = f( x^0 ) = y^0
    double x_00 = tx[0 * (q + 1) + 0];  // x_0^0 - value of x[0] for zero order mode
    double x_10 = tx[1 * (q + 1) + 0];  // x_1^0 - value of x[1] for zero order mode

    fp_0 = 1.0 - pow(std::tanh(x_00 + x_10), 2);  // partial F w.r.t. x_0^0
    fp_1 = 1.0 - pow(std::tanh(x_00 + x_10), 2);  // partial F w.r.t. x_0^1
    px[0] = py[0] * fp_0;                         // partial G w.r.t. x_0^0
    px[1] = py[0] * fp_1;                         // partial G w.r.t. x_0^1

    if (q <= 0) return ok;

    // order 2 (q = 1)
    double f_d0d0 = -2.0 * std::tanh(x_00 + x_10) * (1.0 - pow(std::tanh(x_00 + x_10), 2));
    double f_d1d1 = -2.0 * std::tanh(x_00 + x_10) * (1.0 - pow(std::tanh(x_00 + x_10), 2));
    double f_d0d1 = -2.0 * std::tanh(x_00 + x_10) * (1.0 - pow(std::tanh(x_00 + x_10), 2));

    px[0 * (q + 1) + 1] = py[0 * (q + 1) + 1] * fp_0;
    px[0 * (q + 1) + 0] = px[0 * (q + 1) + 1] + py[0 * (q + 1) + 0] * (f_d0d0 * tx[0 * (q + 1) + 1] + f_d0d1 * tx[1 * (q + 1) + 1]);
    px[1 * (q + 1) + 0] = py[0 * (q + 1) + 1] * fp_1;
    px[1 * (q + 1) + 1] = px[1 * (q + 1) + 0] + py[0 * (q + 1) + 0] * (f_d0d1 * tx[0 * (q + 1) + 1] + f_d1d1 * tx[1 * (q + 1) + 1]);

    if (q > 1) return false;

    return ok;
  }

  /* %$$
  $head End Class Definition$$
  $srccode%cpp% */
};  // End of atomic_get_started class

/* %$$
$head Use Atomic Function$$
$srccode%cpp% */

/* %$$
$$ $comment end nospell$$
$end
*/

int main() {
  std::cout << "testing atomic_get_started" << std::endl;

  atomic_get_started afun("atomic_get_started");

  // domain space vector
  size_t n = 2;
  double x_0 = 1.00;
  double x_1 = 2.00;
  CppAD::vector<CppAD::AD<double> > au(n);
  au[0] = x_0;
  au[1] = x_1;

  // declare independent variables and start tape recording
  CppAD::Independent(au);

  // range space vector
  size_t m = 1;
  CppAD::vector<CppAD::AD<double> > ay(m);

  // call user function
  CppAD::vector<CppAD::AD<double> > ax = au;
  afun(ax, ay);

  // create f: u -> y and stop tape recording
  CppAD::ADFun<double> f;
  f.Dependent(au, ay);  // y = f(u)

  std::cout << "value eval: " << CppAD::Value(ay[0]) << std::endl;

  // --------------------------------------------------------------------
  // --------------------------------------------------------------------
  // zero order forward
  //
  CppAD::vector<double> x0(n), y0(m);
  x0[0] = x_0;
  x0[1] = x_1;
  y0 = f.Forward(0, x0);
  std::cout << "value zero order forward: " << y0[0] << std::endl;

  // --------------------------------------------------------------------
  // --------------------------------------------------------------------
  // first order reverse
  //
  CppAD::vector<double> w(m), dw(n);
  w[0] = 1.0;  // extract derivative of first (and only) component
  dw = f.Reverse(1, w);
  std::cout << "value first order reverse: " << dw[0] << " , " << dw[1] << std::endl;

  // --------------------------------------------------------------------
  // --------------------------------------------------------------------
  // second order reverse
  //
  CppAD::vector<double> x1(n), dw2(2 * n);
  for (size_t j = 0; j < n; j++) {
    for (size_t j1 = 0; j1 < n; j1++) {
      x1[j1] = 0.0;
    }
    x1[j] = 1.0;
    // first order forward
    f.Forward(1, x1);
    w[0] = 1.0;
    dw2 = f.Reverse(2, w);
    std::cout << "hes column: " << dw2[0 * 2 + 1] << " , " << dw2[1 * 2 + 1] << std::endl;
  }

  return 0;
}

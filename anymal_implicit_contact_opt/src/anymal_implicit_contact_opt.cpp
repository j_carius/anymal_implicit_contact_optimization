#include <anymal_implicit_contact_opt/anymal_implicit_contact_opt.hpp>
#include <anymal_implicit_contact_opt/foot_height_cost.hpp>
#include <anymal_implicit_contact_opt/foot_unloaded_lift_cost.hpp>

#include <boost/filesystem.hpp>

//#define USE_REGULAR_AD

#ifdef USE_REGULAR_AD
#include <anymal_bear_ct_model/ANYmalWithContact-impl-dynamics.h>
#endif

namespace anymal_implicit_contact_opt {

// define static vars in case someone needs the address
const size_t ANYmalImplicitContactOpt::STATE_DIM;
const size_t ANYmalImplicitContactOpt::CONTROL_DIM;

void ANYmalImplicitContactOpt::initialize() {
  // save the file for later reference
  std::time_t time_now = std::time(nullptr);
  std::stringstream timeStream;
  timeStream << std::put_time(std::localtime(&time_now), "%y-%m-%d_%OH%OM%OS");
  std::string home = getenv("HOME");
  std::string file = __FILE__;
  std::experimental::filesystem::copy_file(file, home + "/.ros/" + timeStream.str() + "_anymal_implicit_contact_opt.cpp");

  ANYmalWithContactD_.reset(new ANYmalWithContactD_t);

#ifdef USE_REGULAR_AD
  using ANYmalWithContactAD_t = ct::models::ANYmal::ANYmalWithContact<ct::core::ADScalar>;
  std::shared_ptr<ANYmalWithContactAD_t> ANYmalWithContactAD(new ANYmalWithContactAD_t);
  using ad_linearizer_t = ct::core::DiscreteSystemLinearizerAD<STATE_DIM, CONTROL_DIM, double>;
  std::shared_ptr<ad_linearizer_t> ANYmalWithContactLinearized(new ad_linearizer_t(ANYmalWithContactAD));
#else
  ANYmalWithContactLinearizedD_.reset(new ANYmalWithContactLinearizedD_t);
#endif

  /*
   * load configuration files
   */
  boost::filesystem::path filePath(__FILE__);
  std::string configPath = filePath.parent_path().parent_path().generic_string() + std::string{"/config/"};
  const std::string configFile = configPath + "solver.info";
  const std::string problemFile = configPath + "problem.info";

  std::experimental::filesystem::copy_file(configFile, home + "/.ros/" + timeStream.str() + "_solver.info");
  std::experimental::filesystem::copy_file(problemFile, home + "/.ros/" + timeStream.str() + "_problem.info");

  state_vector_t x_0;  // initial state
  ct::core::loadMatrix(problemFile, "init_state", x_0);

  ct::core::loadMatrix(problemFile, "term0.weights.x_des", x_f_);

  std::cout << "[ANYmalImplicitContactOpt] Initial state: " << x_0.transpose() << std::endl;
  std::cout << "[ANYmalImplicitContactOpt] Desired final state: " << x_f_.transpose() << std::endl;

  optconSettings_.load(configFile, true, "solver");

  if (optconSettings_.dt != ANYmalWithContactD_->getTimeDiscretization()) {
    std::cout << "[ANYmalImplicitContactOpt] optconSettings_.dt = " << optconSettings_.dt
              << "; sytem dt: " << ANYmalWithContactD_->getTimeDiscretization() << std::endl;
    throw std::runtime_error("Solver and system time discretization do not match.");
  }

  const bool costFunctionVerbose = false;
  costFunction_.reset(new ct::optcon::CostFunctionAnalytical<STATE_DIM, CONTROL_DIM>(problemFile, costFunctionVerbose));

  // change foot velocity penalty into task space
  const auto joint_positions = ANYmalWithContactD_t::getJointPositionsRobcogenFromCtState(x_0);
  auto jacobians = ANYmalWithContactD_->getDynamics().kinematics().robcogen().jacobians();
  const Eigen::Matrix<double, 6, 3> jac_base_LfFoot_inBase_local = jacobians.fr_base_J_fr_LF_FOOT(joint_positions);
  const Eigen::Matrix<double, 6, 3> jac_base_RfFoot_inBase_local = jacobians.fr_base_J_fr_RF_FOOT(joint_positions);
  const Eigen::Matrix<double, 6, 3> jac_base_LhFoot_inBase_local = jacobians.fr_base_J_fr_LH_FOOT(joint_positions);
  const Eigen::Matrix<double, 6, 3> jac_base_RhFoot_inBase_local = jacobians.fr_base_J_fr_RH_FOOT(joint_positions);

  Eigen::Matrix<double, 12, 12> J_feet;
  J_feet.setZero();
  J_feet.block<3, 3>(0, 0) = jac_base_LfFoot_inBase_local.bottomRows(3);
  J_feet.block<3, 3>(3, 3) = jac_base_RfFoot_inBase_local.bottomRows(3);
  J_feet.block<3, 3>(6, 6) = jac_base_LhFoot_inBase_local.bottomRows(3);
  J_feet.block<3, 3>(9, 9) = jac_base_RhFoot_inBase_local.bottomRows(3);

  // auto& Q = std::dynamic_pointer_cast<ct::optcon::TermQuadratic<STATE_DIM, CONTROL_DIM>>(
  //               costFunction_->getIntermediateTermByName("regularization"))
  //               ->getStateWeight();
  // Q.bottomRightCorner<12, 12>() = J_feet.transpose() * Q.bottomRightCorner<12, 12>() * J_feet;

  //
  // using foot_cost_t = anymal_implicit_contact_opt::FootHeightCost<STATE_DIM, CONTROL_DIM, double, double>;
  // std::shared_ptr<foot_cost_t> foot_height_cost_term(new foot_cost_t());
  // foot_height_cost_term->loadConfigFile(problemFile, "foot_height_cost", costFunctionVerbose);
  // costFunction_->addIntermediateTerm(foot_height_cost_term, costFunctionVerbose);
  //
  // using foot_unloaded_lift_cost_t = anymal_implicit_contact_opt::FootUnloadedLiftCost<STATE_DIM, CONTROL_DIM, double,
  // ct::core::ADCGScalar>; std::shared_ptr<foot_unloaded_lift_cost_t> foot_unloaded_lift_cost_term(new foot_unloaded_lift_cost_t());
  // foot_unloaded_lift_cost_term->loadConfigFile(problemFile, "foot_unloaded_lift_cost", costFunctionVerbose);
  // costFunction_->addIntermediateADTerm(foot_unloaded_lift_cost_term, costFunctionVerbose);
  //
  // costFunction_->initialize();  // compiles the AD term

  // add final constraint
  using state_constr_t = ct::optcon::StateConstraint<STATE_DIM, CONTROL_DIM, double>;
  using constr_container_t = ct::optcon::ConstraintContainerAnalytical<STATE_DIM, CONTROL_DIM, double>;

  std::shared_ptr<constr_container_t> constr_container(new constr_container_t());
  // std::shared_ptr<state_constr_t> final_state_constr(new state_constr_t(x_f_, x_f_));
  // constr_container->addTerminalConstraint(final_state_constr, true);

  // joint angles intermediate constraint
  Eigen::VectorXd lb(12), ub(12);
  lb.setConstant(-M_PI / 1.5);
  ub.setConstant(M_PI / 1.5);
  Eigen::VectorXi stateConstraintSparsity(STATE_DIM);
  stateConstraintSparsity.setZero();
  stateConstraintSparsity.segment<12>(6).setOnes();
  std::shared_ptr<state_constr_t> intermediate_state_constr(new state_constr_t(lb, ub, stateConstraintSparsity));
  constr_container->addIntermediateConstraint(intermediate_state_constr, true);

  // joint velocity intermediate constraint
  lb.setConstant(-12.0);
  ub.setConstant(12.0);
  stateConstraintSparsity.setZero();
  stateConstraintSparsity.segment<12>(6 + 12 + 6).setOnes();
  intermediate_state_constr.reset(new state_constr_t(lb, ub, stateConstraintSparsity));
  constr_container->addIntermediateConstraint(intermediate_state_constr, true);

  ct::core::Time tf;  // final time
  ct::core::loadScalar(configFile, "timeHorizon", tf);
  nSteps_ = static_cast<size_t>(optconSettings_.computeK(tf));

  /*
   * initial guess
   */
  ct::core::ControlVector<CONTROL_DIM, double> u_init_guess;
  ct::core::loadMatrix(problemFile, "u_init_guess", u_init_guess);
  u_init_array_.resize(nSteps_);
  u_init_array_.setConstant(u_init_guess);

  ct::core::FeedbackMatrix<STATE_DIM, CONTROL_DIM> K_init;  // init feedback gain matrix
  ct::core::loadMatrix(problemFile, "init_K", K_init);
  K_init_array_ = ct::core::FeedbackArray<STATE_DIM, CONTROL_DIM>(nSteps_, K_init);

/*
 * setup OptCon problem
 */
#ifdef USE_REGULAR_AD
  optconProblem_ = OptConProblem_t(nSteps_, x_0, ANYmalWithContactD_, costFunction_, ANYmalWithContactLinearized);
#else
  optconProblem_ = OptConProblem_t(nSteps_, x_0, ANYmalWithContactD_, costFunction_, ANYmalWithContactLinearizedD_);
#endif

  constexpr bool useBoxConstraints = false;
  if (useBoxConstraints) {
    optconProblem_.setBoxConstraints(constr_container);
  }

  /*
   * initialize solver
   */
  solver_.reset(new NLOptConSolver_t(optconProblem_, optconSettings_));
  setInitialGuess(x_0);
  // auto backend = solver_->getBackend();
  // if (!backend->nominalRollout()) {
  //   std::cout << "WARNING: Initial rollout unstable!" << std::endl;
  // }
}

void ANYmalImplicitContactOpt::setInitialGuess(const state_vector_t& x0) {
  using ctrl_t = NLOptConSolver_t::Policy_t;

  constexpr bool iLQR = true;
  ctrl_t::state_vector_array_t x_ref{nSteps_ + 1, x0};

  if (iLQR) {
    ctrl_t initController(ctrl_t::state_vector_array_t{nSteps_ + 1, x0},
                          ctrl_t::control_vector_array_t{nSteps_, ctrl_t::control_vector_t::Zero()}, K_init_array_,
                          ANYmalWithContactD_->getTimeDiscretization());
    solver_->setInitialGuess(initController);
    const auto prevSettings = solver_->getSettings();
    auto tmpSettings = prevSettings;
    tmpSettings.closedLoopShooting = true;
    tmpSettings.nlocp_algorithm = ct::optcon::NLOptConSettings::NLOCP_ALGORITHM::ILQR;  // ensure full rollout and not just the first shot
    tmpSettings.K_shot = 1;
    solver_->configure(tmpSettings);

    if (!solver_->getBackend()->nominalRollout()) {
      std::cout << "WARNING: Initial rollout unstable!" << std::endl;
    } else {
      solver_->getBackend()->updateCosts();
      std::cout << "cost of init rollout: " << solver_->getBackend()->getCost() << std::endl;
    }
    const auto solution = solver_->getSolution();

    initController.update(
        solution.x_ref(), solution.uff(),
        ct::core::FeedbackArray<STATE_DIM, CONTROL_DIM>(nSteps_, ct::core::FeedbackMatrix<STATE_DIM, CONTROL_DIM>::Zero()),
        solution.time());
    solver_->configure(prevSettings);
    solver_->setInitialGuess(initController);
  } else {
    for (int i = 1; i < x_ref.size(); i++) {
      x_ref[i](0) = x_ref[i - 1](0) + 1.0 / 100.0;
    }

    ctrl_t initController(x_ref, ctrl_t::control_vector_array_t{nSteps_, 5.0 * ctrl_t::control_vector_t::Random()}, K_init_array_,
                          ANYmalWithContactD_->getTimeDiscretization());

    solver_->setInitialGuess(initController);
  }
}

void ANYmalImplicitContactOpt::changeInitialState(const state_vector_t& x0) {
  solver_->changeInitialState(x0);
  std::cout << "[ANYmalImplicitContactOpt] new initial state: " << x0.transpose() << std::endl;
}

void ANYmalImplicitContactOpt::getPercussions(ct::core::DiscreteArray<percussion_t>& percussions) const {
  const auto x_ref = solver_->getSolution().x_ref();
  const auto uff = solver_->getSolution().uff();

  state_vector_t state_next;

  percussions.reserve(uff.size());

  for (size_t i = 0; i < uff.size(); ++i) {
    percussion_t P;
    ANYmalWithContactD_->propagateControlledDynamicsExtended(x_ref[i], i, uff[i], state_next, P);
    assert(state_next.isApprox(x_ref[i + 1], 1e-8));  // TODO just sanity check, can be removed
    percussions.push_back(P);
  }
}

void ANYmalImplicitContactOpt::updateContinuation() {
  auto& Q1 = std::dynamic_pointer_cast<ct::optcon::TermQuadratic<STATE_DIM, CONTROL_DIM>>(
                 costFunction_->getIntermediateTermByName("trot leg 1 and 4 cost"))
                 ->getStateWeight();  // *= 0.9;
  Q1 *= 0.99;
  auto& Q2 = std::dynamic_pointer_cast<ct::optcon::TermQuadratic<STATE_DIM, CONTROL_DIM>>(
                 costFunction_->getIntermediateTermByName("trot leg 2 and 3 cost"))
                 ->getStateWeight();  // *= 0.9;
  Q2 *= 0.99;
  std::cout << "updated trot cost " << Q1(6, 6) << std::endl;
  solver_->changeCostFunction(costFunction_);
}

bool ANYmalImplicitContactOpt::runIteration() {
  bool foundBetter = solver_->runIteration();
  // updateContinuation();
  return foundBetter;
}

void ANYmalImplicitContactOpt::solveDMS() {
  // ct::optcon::DmsSettings dmsSettings;
  //
  // dmsSettings.solverSettings_.solverType_ = ct::optcon::NlpSolverSettings::IPOPT;
  // dmsSettings.solverSettings_.ipoptSettings_.max_iter_ = 200;
  // // dmsSettings.solverSettings_.ipoptSettings_.derivativeTest_ = "first-order";
  // // dmsSettings.solverSettings_.ipoptSettings_.checkDerivativesForNaninf_ = "yes";
  // // dmsSettings.solverSettings_.ipoptSettings_.derivativeTestPrintAll_ = "yes";
  // // dmsSettings.solverSettings_.ipoptSettings_.derivativeTestTol_ = 1e-5;
  // dmsSettings.dt_sim_ = optconSettings_.dt;
  // dmsSettings.N_ = optconProblem_.getTimeHorizon();       // number of shots
  // dmsSettings.T_ = dmsSettings.dt_sim_ * dmsSettings.N_;  // time horizon
  // dmsSettings.nThreads_ = 1;
  //
  // dmsSolver_.reset(new DmsSolver_t(optconProblem_, dmsSettings));
  //
  // ct::optcon::DmsPolicy<STATE_DIM, CONTROL_DIM, double> initGuess;
  // initGuess.xSolution_.resize(dmsSettings.N_ + 1);
  // initGuess.uSolution_.resize(dmsSettings.N_ + 1);  // unclear why +1 is needed
  // for (auto& x : initGuess.xSolution_) {
  //   x.setRandom();
  // }
  // initGuess.xSolution_[0](0) = -1.0;
  // for (int i = 1; i < initGuess.xSolution_.size(); i++) {
  //   initGuess.xSolution_[i](0) = initGuess.xSolution_[i - 1](0) + 1.0 / 100.0;
  // }
  // for (auto& u : initGuess.uSolution_) {
  //   u.setRandom();
  // }
  // dmsSolver_->setInitialGuess(initGuess);
  //
  // dmsSolver_->solve();
}

void ANYmalImplicitContactOpt::runMPC(const int timeSteps) {
  if (solver_ == nullptr) {
    std::cout << "call initialize before runMPC" << std::endl;
    return;
  }

  std::cout << "[runMPC] solving initially for convergence." << std::endl;
  // first, solve the normal problem to convergence
  if (!solve()) {
    std::cout << "[runMPC] something went wrong while solving." << std::endl;
    return;
  }

  auto dt = ANYmalWithContactD_->getTimeDiscretization();
  Solution_t mpc_solution;

  for (int mpcStep = 0; mpcStep < timeSteps; mpcStep++) {
    std::cout << "[runMPC] running MPC step " << mpcStep << std::endl;

    // run one iteration
    solver_->runIteration();

    // save
    auto curr_solution = solver_->getSolution();  // unfortunately need to make a copy
    mpc_solution.getReferenceStateTrajectory().push_back(curr_solution.getReferenceStateTrajectory()[0], dt, mpcStep ? false : true);
    mpc_solution.getFeedforwardTrajectory().push_back(curr_solution.getFeedforwardTrajectory()[0], dt, mpcStep ? false : true);
    mpc_solution.getFeedbackTrajectory().push_back(curr_solution.getFeedbackTrajectory()[0], dt, mpcStep ? false : true);

    // // adapt the final goal by same amount as the motion in the first time step
    // auto x_ref =
    //     std::dynamic_pointer_cast<ct::optcon::TermQuadratic<STATE_DIM, CONTROL_DIM>>(costFunction_->getIntermediateTermByName("final"))
    //         ->getReferenceState();
    // x_ref(0) += curr_solution.getReferenceStateTrajectory()[1](0) - curr_solution.getReferenceStateTrajectory()[0](0);

    // shift current policy (delete first element, repeat last element)
    curr_solution.getReferenceStateTrajectory().eraseFront(1);
    curr_solution.getReferenceStateTrajectory().push_back(curr_solution.getReferenceStateTrajectory().back(), dt, false);
    curr_solution.getFeedforwardTrajectory().eraseFront(1);
    curr_solution.getFeedforwardTrajectory().push_back(curr_solution.getFeedforwardTrajectory().back(), dt, false);
    curr_solution.getFeedbackTrajectory().eraseFront(1);
    curr_solution.getFeedbackTrajectory().push_back(curr_solution.getFeedbackTrajectory().back(), dt, false);

    // update init guess
    solver_->setInitialGuess(curr_solution);
  }

  // save solution to solver so it can be extracted as normal (by bagwriter)
  mpc_solution.getFeedforwardTrajectory().pop_back();
  mpc_solution.getFeedbackTrajectory().pop_back();
  solver_->changeTimeHorizon(dt * (timeSteps - 1));
  solver_->setInitialGuess(mpc_solution);
}

}  // namespace anymal_implicit_contact_opt

#include <anymal_implicit_contact_opt/anymal_implicit_contact_opt.hpp>

#include <es.h>
#include <eo/eo>

using anymalOpt_t = anymal_implicit_contact_opt::ANYmalImplicitContactOpt;
const size_t STATE_DIM = anymalOpt_t::STATE_DIM;
const size_t CONTROL_DIM = anymalOpt_t::CONTROL_DIM;
using costFct_t = ct::optcon::CostFunctionAnalytical<STATE_DIM, CONTROL_DIM>;
using termQ_t = ct::optcon::TermQuadratic<STATE_DIM, CONTROL_DIM>;

// EO: Define individuals (representation)
typedef eoReal<double> Indi;

// EO: Eval function
double eval_opt(const Indi& indi) {
  std::shared_ptr<anymalOpt_t> anymalOpt(new anymalOpt_t());  // put it on the heap
  anymalOpt->initialize();

  // create new quadratic analytical cost function
  ct::core::StateVector<STATE_DIM> Q;
  ct::core::ControlVector<CONTROL_DIM> R;

  for (size_t i = 0; i < STATE_DIM; i++) {
    Q(i) = indi[i];
  }
  for (size_t i = 0; i < CONTROL_DIM; i++) {
    R(i) = indi[STATE_DIM + i];
  }

  std::shared_ptr<costFct_t> cf(new costFct_t());
  std::shared_ptr<termQ_t> termQ(new termQ_t(Q.asDiagonal(), R.asDiagonal()));

  ct::core::StateVector<STATE_DIM> x_ref;
  ct::core::ControlVector<CONTROL_DIM> u_ref;
  u_ref.setZero();
  x_ref.setZero();
  x_ref(2) = 0.41;
  x_ref.segment<12>(6) << 0.1, 0.7, -1.0, -0.1, 0.7, -1.0, 0.1, -0.7, 1.0, -0.1, -0.7, 1.0;

  termQ->setStateAndControlReference(x_ref, u_ref);

  cf->addIntermediateTerm(termQ);

  anymalOpt->changeCostFunction(cf);

  anymalOpt->solve();

  anymalOpt_t::Solution_t solution;
  anymalOpt->getSolution(solution);
  return solution.x_ref().back()(0);  // EO only maximizes
}

int main(int argc, char* argv[]) {
  // all EO

  // most parameters are hard-coded!
  const unsigned int SEED = 42;                           // seed for random number generator
  const unsigned int VEC_SIZE = STATE_DIM + CONTROL_DIM;  // Number of object variables in genotypes
  const unsigned int POP_SIZE = 200;                      // Size of population
  const unsigned int T_SIZE = 3;                          // size for tournament selection
  const unsigned int MAX_GEN = 10;                        // Maximum number of generation before STOP
  const float CROSS_RATE = 0.8;                           // Crossover rate
  const double EPSILON = 0.2;                             // range for real uniform mutation
  const float MUT_RATE = 0.5;                             // mutation rate

  // need parser to set parallel option
  char argv0[] = "genetic_cost_search";
  char argv1[] = "--parallelize-loop=0";
  char argv2[] = "--parallelize-do-measure=1";
  char argv3[] = "--parallelize-nthreads=2";  // 0 means max
  char* argv_dummy[] = {argv0, argv1, argv2, argv3};
  const int argc_dummy = 4;

  eoParser parser(argc_dummy, argv_dummy);
  make_parallel(parser);
  std::cout << "eoparallel: " << eo::parallel.isEnabled() << std::endl;

  // Random seed
  rng.reseed(SEED);

  // Evaluation: from a plain C++ fn to an EvalFunc Object
  eoEvalFuncPtr<Indi> eval(eval_opt);

  // declare and initialize the population
  eoPop<Indi> pop;
  pop.resize(POP_SIZE);

#pragma omp parallel for if (eo::parallel.isEnabled())
  for (unsigned int igeno = 0; igeno < POP_SIZE; igeno++) {
    Indi v;  // void individual, to be filled
    for (unsigned ivar = 0; ivar < VEC_SIZE; ivar++) {
      double r = rng.uniform();  // new value, random in [0,1)
      v.push_back(r);            // append that random value to v
    }
    // eval(v);         // evaluate it
    pop[igeno] = v;  // and put it in the population
  }

  std::cout << "done with initialization of population" << std::endl;
  std::cout << "pop:\n" << pop << std::endl;

#pragma omp parallel for if (eo::parallel.isEnabled())
  for (unsigned int igeno = 0; igeno < POP_SIZE; igeno++) {
    eval(pop[igeno]);
  }
  std::cout << "done with evaluating initial of population" << std::endl;
  std::cout << "pop:\n" << pop << std::endl;

  // ENGINE
  /////////////////////////////////////
  // selection and replacement
  ////////////////////////////////////
  // SELECT
  // The robust tournament selection
  eoDetTournamentSelect<Indi> select(T_SIZE);  // T_SIZE in [2,POP_SIZE]

  // REPLACE
  // eoSGA uses generational replacement by default
  // so no replacement procedure has to be given

  // OPERATORS
  //////////////////////////////////////
  // The variation operators
  //////////////////////////////////////
  // CROSSOVER
  // offspring(i) is a linear combination of parent(i)
  eoSegmentCrossover<Indi> xover;
  // MUTATION
  // offspring(i) uniformly chosen in [parent(i)-epsilon, parent(i)+epsilon] within bounds

  const double maxBound = 1e10;
  eoRealVectorBounds bounds(VEC_SIZE, 0, maxBound);
  eoUniformMutation<Indi> mutation(bounds, std::sqrt(EPSILON / maxBound));

  // STOP
  // CHECKPOINT
  //////////////////////////////////////
  // termination condition
  /////////////////////////////////////
  // stop after MAX_GEN generations
  eoGenContinue<Indi> continuatorMaxGen(MAX_GEN);
  eoCtrlCContinue<Indi> continuatorCtrlC;
  eoCombinedContinue<Indi> continuator(continuatorMaxGen);
  continuator.add(continuatorCtrlC);

  // GENERATION
  /////////////////////////////////////////
  // the algorithm
  ////////////////////////////////////////
  // standard Generational GA requires
  // selection, evaluation, crossover and mutation, stopping criterion

  eoSGA<Indi> gga(select, xover, CROSS_RATE, mutation, MUT_RATE, eval, continuator);

  // Apply algo to pop - that's it!
  gga(pop);

  // OUTPUT
  // Print (sorted) population. First is best
  pop.sort();
  std::cout << "FINAL Population (first is best)\n" << pop << std::endl;
  std::cout << "Best individual:\n" << pop[0] << std::endl;
  double final_score = eval_opt(pop[0]);
  std::cout << "\n\n score in EO: " << final_score << std::endl;
  // GENERAL
}

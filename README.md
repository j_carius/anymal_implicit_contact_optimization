# ANYmal Implicit Contact Optimization

Author: Jan Carius, jcarius@ethz.ch

## Building
```
catkin build anymal_implicit_contact_opt_ros --cmake-args -DUSE_CLANG=ON
```
Note that clang should be used if the model shared library of `anymal_bear_ct_model` was also built with clang.

## Execution
For testing the forward simulation, first generate a bag file with the forward simulated ANYmal model.
```
rosrun anymal_implicit_contact_opt_ros anymal_implicit_contact_opt_ros_fwdSim
```
The bag should be saved in the current directly of the console.
Its contents can be visualized with
```
roslaunch anymal_implicit_contact_opt_ros visualize_bag.launch bag_filename:=`pwd`/anymal_traj.bag
```
For this to work, the packages `quadruped_tf_publisher`, `anymal_bear_description`, and `anymal_bear_common` need to be built and sourced.

To run the optimization
```
rosrun anymal_implicit_contact_opt_ros bagWriter
```